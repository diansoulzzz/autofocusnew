package com.autofocus.www.autofocusnew.app.register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.login.LoginActivity;
import com.autofocus.www.autofocusnew.network.Service;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by GBS on 17/01/2018.
 */

public class RegisterActivity extends AppBase implements RegisterView {
    @BindView(R.id.txtName)
    EditText txtName;
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    @BindView(R.id.container)
    RelativeLayout container;
    @BindView(R.id.form_container)
    LinearLayout formContainer;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.txtConPassword)
    EditText txtConPassword;
    @Inject
    public Service service;
    RegisterPresenter presenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_register);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        ButterKnife.bind(this);
        presenter = new RegisterPresenter(service, this, preferences);
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(getApplicationContext(),appErrorMessage,Toast.LENGTH_LONG).show();
    }

    @Override
    public void startNextActivity()
    {
        finish();
        setResult(1);
    }

    @Override
    public void showNameError(CharSequence error) {
        txtName.setError(error);
    }

    @Override
    public void showEmailError(CharSequence error) {
        txtEmail.setError(error);
    }

    @Override
    public void showPasswordError(CharSequence error) {
        txtPassword.setError(error);
    }

    @Override
    public void showconPasswordEror(CharSequence eror) {
        txtConPassword.setError(eror);
    }

    @OnClick(R.id.btnRegister)
    void register() {
//        String checkpassword = txtPassword.getText().toString();
        boolean error = false;
        if (!txtName.getText().toString().matches("[a-zA-Z.? 0-9]*"))
        {
            error = true;
            showNameError("Name can't contain special character");
        }
        if (!txtEmail.getText().toString().matches("[a-zA-Z.? 0-9@]*"))
        {
            error = true;
            showEmailError("Email can't contain special character");
        }
        if (!txtPassword.getText().toString().matches("[a-zA-Z.? 0-9]*"))
        {
            error = true;
            showPasswordError("Password can't contain special character");
        }

        if (!error)
        {
            presenter.attemptRegister(txtName.getText().toString(),txtEmail.getText().toString(),txtPassword.getText().toString(), txtConPassword.getText().toString());
        }
    }

    @OnClick(R.id.btnToLogin)
    void goToLogin() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
    }
}
