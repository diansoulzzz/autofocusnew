package com.autofocus.www.autofocusnew.app.changepassword;

import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;

public interface ChangePasswordView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void setOldPassword(String text);

    void setNewPassword(String text);

    void setConfirmPassword(String text);

    void showOldPasswordError(CharSequence error);

    void showNewPasswordError(CharSequence error);

    void showConfirmPasswordError(CharSequence error);

    void finishActivity();
}
