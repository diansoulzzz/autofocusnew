package com.autofocus.www.autofocusnew.network;

import com.autofocus.www.autofocusnew.models.AccessToken;
import com.autofocus.www.autofocusnew.models.CityListResponse;
import com.autofocus.www.autofocusnew.models.PostingDataResponse;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.models.UserDataResponse;
import com.autofocus.www.autofocusnew.models.UserResponse;
import com.google.firebase.auth.GetTokenResult;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Service {
    private final NetworkService networkService;
    private final String TAG = "Service";
//    TokenManager tokenManager;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription getCityList(final GetCityListCallback callback) {
        return networkService.getCityList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends CityListResponse>>() {
                    @Override
                    public Observable<? extends CityListResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<CityListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(CityListResponse cityListResponse) {
                        callback.onSuccess(cityListResponse);

                    }
                });
    }

    public Subscription getPostingList(final GetPostingListCallback callback, String token) {
        return networkService.getPostingList("Bearer " + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PostingResponse>>() {
                    @Override
                    public Observable<? extends PostingResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PostingResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(PostingResponse response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription addPosting(final GetPostingListCallback callback, String token, String caption, Double lat, Double lng, String photourl, String location, String camera_maker, String camera_model, String fstop, String exposure_time, String isospeed) {
        return networkService.addPosting("Bearer " + token, caption, lat, lng, photourl, location, camera_maker, camera_model, fstop, exposure_time, isospeed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PostingResponse>>() {
                    @Override
                    public Observable<? extends PostingResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PostingResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(PostingResponse response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription getUserList(final GetUserListCallback callback, String token) {
        return networkService.getUserList("Bearer " + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends UserResponse>>() {
                    @Override
                    public Observable<? extends UserResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<UserResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(UserResponse response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription getUserProfile(final GetUserCallback callback, String token) {
        return networkService.getUserProfile("Bearer " + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends UserDataResponse>>() {
                    @Override
                    public Observable<? extends UserDataResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<UserDataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(UserDataResponse response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription changePassword(final GetAccessTokenCallBack callback, String token, String old_password, String new_password, String confirm_password) {
        return networkService.changePassword("Bearer " + token, old_password, new_password, confirm_password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AccessToken>>() {
                    @Override
                    public Observable<? extends AccessToken> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<AccessToken>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(AccessToken response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription setUserProfile(final GetUserCallback callback, String token, String name, String gender, String biodata, String alamat) {
        return networkService.setUserProfile("Bearer " + token, name, gender, biodata, alamat)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends UserDataResponse>>() {
                    @Override
                    public Observable<? extends UserDataResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<UserDataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(UserDataResponse response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription setProfileImage(final GetUserCallback callback, String token, String photourl) {
        return networkService.setImageProfile("Bearer " + token, photourl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends UserDataResponse>>() {
                    @Override
                    public Observable<? extends UserDataResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<UserDataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(UserDataResponse response) {
                        callback.onSuccess(response);

                    }
                });
    }
    public Subscription toogleFollowGetUserList(final GetUserListCallback callback, String token, Integer users_id) {
        return networkService.toogleFollowGetUserList("Bearer " + token, users_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends UserResponse>>() {
                    @Override
                    public Observable<? extends UserResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<UserResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(UserResponse response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription getPostingWhereId(final GetPostingCallback callback, String token, Integer posting_id) {
        return networkService.getPostingWhereId("Bearer " + token, posting_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PostingDataResponse>>() {
                    @Override
                    public Observable<? extends PostingDataResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PostingDataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(PostingDataResponse response) {
                        callback.onSuccess(response);
                    }
                });
    }

    public Subscription setPostingRating(final GetPostingCallback callback, String token, Integer posting_id, Float stars) {
        return networkService.setPostingRating("Bearer " + token, posting_id, stars)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PostingDataResponse>>() {
                    @Override
                    public Observable<? extends PostingDataResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PostingDataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(PostingDataResponse response) {
                        callback.onSuccess(response);
                    }
                });
    }

    public Subscription addCommentGetPosting(final GetPostingCallback callback, String token, Integer posting_id, String comment) {
        return networkService.addCommentGetPosting("Bearer " + token, posting_id, comment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PostingDataResponse>>() {
                    @Override
                    public Observable<? extends PostingDataResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PostingDataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(PostingDataResponse response) {
                        callback.onSuccess(response);
                    }
                });
    }

    public Subscription getTokenLogin(final GetAccessTokenCallBack callback, String username, String password) {
        return networkService.login(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AccessToken>>() {
                    @Override
                    public Observable<? extends AccessToken> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<AccessToken>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(AccessToken response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription getRefreshToken(final GetAccessTokenCallBack callback, String refreshToken) {
        return networkService.refresh(refreshToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AccessToken>>() {
                    @Override
                    public Observable<? extends AccessToken> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<AccessToken>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(AccessToken response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public Subscription getTokenRegister(final GetAccessTokenCallBack callback, String name, String username, String password, String conPassword) {
        return networkService.register(name, username, password, conPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AccessToken>>() {
                    @Override
                    public Observable<? extends AccessToken> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<AccessToken>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(AccessToken response) {
                        callback.onSuccess(response);

                    }
                });
    }

    public interface GetCityListCallback {
        void onSuccess(CityListResponse response);

        void onError(NetworkError networkError);
    }

    public interface GetPostingListCallback {
        void onSuccess(PostingResponse response);

        void onError(NetworkError networkError);
    }

    public interface GetUserListCallback {
        void onSuccess(UserResponse response);

        void onError(NetworkError networkError);
    }

    public interface GetPostingCallback {
        void onSuccess(PostingDataResponse response);

        void onError(NetworkError networkError);
    }

    public interface GetUserCallback {
        void onSuccess(UserDataResponse response);

        void onError(NetworkError networkError);
    }

    public interface GetAccessTokenCallBack {
        void onSuccess(AccessToken response);

        void onError(NetworkError networkError);
    }
}