package com.autofocus.www.autofocusnew.app.main.home;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.models.Posting;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragmentAdapter extends
        RecyclerView.Adapter<HomeFragmentAdapter.ViewHolder> {

    private static final int VIEW_TYPE_POSTING = 0;
    private static final int VIEW_TYPE_EMPTY = 2;

//    private ArrayList items;

    private final HomeFragmentAdapter.OnItemClickListener listener;
    private final HomeFragmentAdapter.OnBtnCommentClickListener btnCommentClickListener;
    private final HomeFragmentAdapter.OnBtnFollowClickListener btnFollowClickListener;
    private final HomeFragmentAdapter.OnBtnPostingClickListener btnPostingClickListener;
    private List<Posting> data;
    private Context context;

    public HomeFragmentAdapter(Context context, List<Posting> data, OnItemClickListener listener, OnBtnCommentClickListener btnCommentClickListener, OnBtnFollowClickListener btnFollowClickListener, OnBtnPostingClickListener btnPostingClickListener) {
        this.data = data;
        this.listener = listener;
        this.btnCommentClickListener = btnCommentClickListener;
        this.context = context;
        this.btnFollowClickListener = btnFollowClickListener;
        this.btnPostingClickListener = btnPostingClickListener;
    }

    @Override
    public int getItemCount() {
        if(data.size() == 0){
            return 1;
        }else {
            return data.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (data.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }else{
            return VIEW_TYPE_POSTING;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        ViewHolder vh;
        if (viewType == VIEW_TYPE_POSTING) {
            v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_posting, parent, false);
            vh = new ViewHolderPosting(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_home_empty, parent, false);
            vh = new ViewHolderEmpty(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(HomeFragmentAdapter.ViewHolder viewHolder, final int position) {
        int viewType = getItemViewType(position);
        if (viewType == VIEW_TYPE_POSTING) {
            ViewHolderPosting holder = (ViewHolderPosting) viewHolder;
            holder.click(data.get(position), listener);
            holder.rabStarsclick(data.get(position), holder, listener);
            holder.btnCommentclick(data.get(position), btnCommentClickListener);
            holder.tvPostingCaption.setText(data.get(position).getCaption());
            holder.tvPostingLocation.setText(data.get(position).getLocation());
            holder.tvProfileName.setText(data.get(position).getUser().getName());
            holder.tvTotalRating.setText(String.format(Locale.ENGLISH, "%.1f", data.get(position).getTotalRating()));
            holder.rabStars.setRating(data.get(position).getCurrentUserRating());
            String postingImage = data.get(position).getPhotourl();
            String profileImage = data.get(position).getUser().getPhotourl();
            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true);
            Glide.with(context)
                    .load(postingImage)
                    .apply(requestOptions)
                    .into(holder.imgPosting);
            Glide.with(context)
                    .load(profileImage)
                    .apply(requestOptions.apply(RequestOptions.circleCropTransform()))
                    .into(holder.imgProfile);
        } else if (viewType == VIEW_TYPE_EMPTY) {
            ViewHolderEmpty holder = (ViewHolderEmpty) viewHolder;
            holder.btnFollowclick(btnFollowClickListener);
            holder.btnPostingclick(btnPostingClickListener);
        }
    }

    public interface OnItemClickListener {
        void onClick(Posting Item);
        void onStarsChoose(Posting Item, Float rating, HomeFragmentAdapter.ViewHolderPosting holder);
    }

    public interface OnBtnCommentClickListener {
        void onClick(Posting Item);
    }
    public interface OnBtnFollowClickListener {
        void onClick();
    }
    public interface OnBtnPostingClickListener {
        void onClick();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public static class ViewHolderPosting extends ViewHolder {

        public ViewHolderPosting(View v) {
            super(v);
            ButterKnife.bind(this, itemView);
        }
        @Nullable
        @BindView(R.id.tvPostingCaption)
        TextView tvPostingCaption;
        @Nullable
        @BindView(R.id.tvPostingLocation)
        TextView tvPostingLocation;
        @Nullable
        @BindView(R.id.tvProfileName)
        TextView tvProfileName;
        @Nullable
        @BindView(R.id.tvTotalRating)
        TextView tvTotalRating;
        @Nullable
        @BindView(R.id.imgPosting)
        ImageView imgPosting;
        @Nullable
        @BindView(R.id.imgProfile)
        ImageView imgProfile;
        @Nullable
        @BindView(R.id.rabStars)
        RatingBar rabStars;
        @Nullable
        @BindView(R.id.btnComment)
        Button btnComment;

        public void btnCommentclick(final Posting data, final HomeFragmentAdapter.OnBtnCommentClickListener listener) {
            btnComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(data);
                }
            });
        }

        public void rabStarsclick(final Posting data, final HomeFragmentAdapter.ViewHolderPosting holder, final HomeFragmentAdapter.OnItemClickListener listener) {
            rabStars.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    listener.onStarsChoose(data, rating,holder);
                }
            });
        }
        public void click(final Posting data, final HomeFragmentAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (rabStars.getVisibility() == View.GONE) {
                        rabStars.setVisibility(View.VISIBLE);
                    } else {
                        rabStars.setVisibility(View.GONE);
                    }
                    listener.onClick(data);
                }
            });
        }
    }

    public static class ViewHolderEmpty extends ViewHolder {

        public ViewHolderEmpty(View v) {
            super(v);
            ButterKnife.bind(this, itemView);
        }
        @Nullable
        @BindView(R.id.btnFollow)
        Button btnFollow;

        @Nullable
        @BindView(R.id.btnPosting)
        Button btnPosting;

        public void btnFollowclick(final HomeFragmentAdapter.OnBtnFollowClickListener listener) {
            btnFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick();
                }
            });
        }

        public void btnPostingclick(final HomeFragmentAdapter.OnBtnPostingClickListener listener) {
            btnPosting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick();
                }
            });
        }
    }

}