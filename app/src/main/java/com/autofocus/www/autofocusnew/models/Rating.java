package com.autofocus.www.autofocusnew.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rating {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("posting_id")
    @Expose
    private Integer postingId;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("bintang")
    @Expose
    private Integer bintang;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPostingId() {
        return postingId;
    }

    public void setPostingId(Integer postingId) {
        this.postingId = postingId;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public Integer getBintang() {
        return bintang;
    }

    public void setBintang(Integer bintang) {
        this.bintang = bintang;
    }

}