package com.autofocus.www.autofocusnew.network;

import com.google.gson.annotations.SerializedName;

public class NetworkResponse {
    @SerializedName("status")
    public String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @SuppressWarnings({"unused", "used by Retrofit"})
    public NetworkResponse() {
    }

    public NetworkResponse(String status) {
        this.status = status;
    }
}