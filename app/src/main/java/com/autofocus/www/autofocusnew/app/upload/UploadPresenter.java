package com.autofocus.www.autofocusnew.app.upload;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.media.ExifInterface;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.PostingDataResponse;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.models.UserDataResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

public class UploadPresenter {
    private final Service service;
    private final UploadView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "UploadPresenter";
    private TokenManager tokenManager;
    private final String filepath;
    private FirebaseStorage firebaseStorage;

    public UploadPresenter(Service service, UploadView view, SharedPreferences preferences, String filepath, FirebaseStorage firebaseStorage) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
        this.filepath = filepath;
        this.firebaseStorage = firebaseStorage;
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

    public void setAttribute() {
        view.setPreviewImage(filepath);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filepath);
            if (exif.getAttribute(ExifInterface.TAG_MAKE) == null) {
                view.setCameraMakerText(Build.BRAND);
                view.setCameraModelText(Build.MODEL);
            } else {
                view.setCameraMakerText(exif.getAttribute(ExifInterface.TAG_MAKE));
                view.setCameraModelText(exif.getAttribute(ExifInterface.TAG_MODEL));
                view.setCameraIsoText(exif.getAttribute(ExifInterface.TAG_ISO_SPEED_RATINGS));
                view.setCameraExposureText(exif.getAttribute(ExifInterface.TAG_EXPOSURE_TIME));
                view.setCameraFstopText(exif.getAttribute(ExifInterface.TAG_F_NUMBER));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath){
//        int rotate = 0;
//        try {
//            context.getContentResolver().notifyChange(imageUri, null);
//            File imageFile = new File(imagePath);
//            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//            switch (orientation) {
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    rotate = 270;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    rotate = 180;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    rotate = 90;
//                    break;
//            }
//            Log.i("RotateImage", "Exif orientation: " + orientation);
//            Log.i("RotateImage", "Rotate value: " + rotate);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return rotate;
//    }

    public void getUserProfile() {
        view.showWait();
        Subscription subscription = service.getUserProfile(new Service.GetUserCallback() {
            @Override
            public void onSuccess(UserDataResponse response) {
                RequestOptions requestOptions = new RequestOptions();
                RequestListener listener = new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        view.removeWait();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        view.removeWait();
                        return false;
                    }
                };
                view.setProfileImage(requestOptions, listener, response.getData().getPhotourl());
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
            }

        }, tokenManager.getToken().getAccessToken());
        subscriptions.add(subscription);
    }

    public static String getFileType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public void uploadImage(OnFailureListener failureListener, OnSuccessListener<UploadTask.TaskSnapshot> successListener) {
//        view.showWait();
        StorageMetadata metadata = new StorageMetadata.Builder().setContentType(getFileType(filepath)).build();
        String uniqueId = UUID.randomUUID().toString();
        StorageReference imagesRef = firebaseStorage.getReference().child("posting");
        StorageReference fileRef = imagesRef.child(uniqueId);
        BitmapFactory.Options options = new BitmapFactory.Options();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BitmapFactory.decodeFile(filepath, options).compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = fileRef.putBytes(data, metadata);
        uploadTask.addOnFailureListener(failureListener).addOnSuccessListener(successListener);
//        uploadTask.addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//
//            }
//        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//            }
//        });
    }

    public void sendPosting(String caption, Double lat, Double lng, String photourl, String location, String camera_maker, String camera_model, String fstop, String exposure_time, String isospeed)
    {
        Subscription subscription = service.addPosting(new Service.GetPostingListCallback() {
            @Override
            public void onSuccess(PostingResponse response) {
                Log.d(TAG, "onSuccess");

                view.removeWait();
                view.clearActivity();
                view.onFailure("photo was uploaded");
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
//                if (errorResponse.getErrors() != null)
//                {
//                    for (Map.Entry<String, List<String>> error : errorResponse.getErrors().entrySet()) {
//                        if (error.getKey().equals("name")) {
//                            view.showNameError(error.getValue().get(0));
//                        }
//                        if (error.getKey().equals("email")) {
//                            view.showEmailError(error.getValue().get(0));
//                        }
//                        if (error.getKey().equals("password")) {
//                            view.showPasswordError(error.getValue().get(0));
//                        }
//                    }
//                }
            }
        }, tokenManager.getToken().getAccessToken(), caption, lat, lng, photourl, location, camera_maker, camera_model, fstop, exposure_time, isospeed);
        subscriptions.add(subscription);
    }
    public void addPosting(final String caption, final Double lat, final Double lng, final String location, final String camera_maker, final String camera_model, final String fstop, final String exposure_time, final String isospeed) {
        view.showWait();
        this.uploadImage(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                view.removeWait();
            }
        }, new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                sendPosting(caption,lat,lng,taskSnapshot.getDownloadUrl().toString(),location,camera_maker,camera_model,fstop,exposure_time,isospeed);
            }
        });

    }
}
