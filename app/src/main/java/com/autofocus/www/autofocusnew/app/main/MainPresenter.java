package com.autofocus.www.autofocusnew.app.main;

import android.content.SharedPreferences;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import rx.subscriptions.CompositeSubscription;

public class MainPresenter {
    private final Service service;
    private final MainView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "MainPresenter";
    private TokenManager tokenManager;

    public MainPresenter(Service service, MainView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

}
