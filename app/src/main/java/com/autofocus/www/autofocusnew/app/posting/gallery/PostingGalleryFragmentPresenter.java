package com.autofocus.www.autofocusnew.app.posting.gallery;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.provider.MediaStore;

import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import java.util.ArrayList;
import java.util.List;

import rx.subscriptions.CompositeSubscription;

public class PostingGalleryFragmentPresenter {
    private final Service service;
    private final PostingGalleryFragmentView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "SearchUserPresenter";
    private TokenManager tokenManager;

    public PostingGalleryFragmentPresenter(Service service, PostingGalleryFragmentView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }
    public List<String> getImagePaths(Cursor cursor) {
        List<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int image_path_col = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                result.add(cursor.getString(image_path_col));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }
}
