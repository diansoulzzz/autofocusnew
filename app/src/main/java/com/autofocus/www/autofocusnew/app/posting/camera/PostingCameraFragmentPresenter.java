package com.autofocus.www.autofocusnew.app.posting.camera;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.UserResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import java.io.File;
import java.io.FileOutputStream;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class PostingCameraFragmentPresenter {
    private final Service service;
    private final PostingCameraFragmentView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "SearchUserPresenter";
    private TokenManager tokenManager;

    public PostingCameraFragmentPresenter(Service service, PostingCameraFragmentView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }

    public void saveImage(Bitmap finalBitmap)
    {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        String fname = "Image-Temp" + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            view.startNextActivity(file.getPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }
}
