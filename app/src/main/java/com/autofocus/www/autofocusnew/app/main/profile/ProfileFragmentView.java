package com.autofocus.www.autofocusnew.app.main.profile;

import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.models.UserDataResponse;
import com.bumptech.glide.request.RequestOptions;

public interface ProfileFragmentView {

    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getPostingList(UserDataResponse response);

    void setProfileImage(RequestOptions requestOptions, String imageurl);

    void setProfileLocal(RequestOptions requestOptions, String path);

    void setPostingCount(String count);

    void setFollowerCount(String count);

    void setFollowingCount(String count);

    void setProfileEmail(String email);

    void setProfileName(String name);

    void setProfileBio(String bio);

    void setProfileAdress(String adress);

    void clearImage();
}
