
package com.autofocus.www.autofocusnew.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Posting {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("photourl")
    @Expose
    private String photourl;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("camera_maker")
    @Expose
    private String cameraMaker;
    @SerializedName("camera_model")
    @Expose
    private String cameraModel;
    @SerializedName("fstop")
    @Expose
    private String fstop;
    @SerializedName("exposure_time")
    @Expose
    private String exposureTime;
    @SerializedName("isospeed")
    @Expose
    private String isospeed;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("total_rating")
    @Expose
    private Float totalRating;
    @SerializedName("current_user_rating")
    @Expose
    private Float currentUserRating;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("komentars")
    @Expose
    private List<Komentar> komentars = null;
    @SerializedName("ratings")
    @Expose
    private List<Rating> ratings = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getPhotourl() {
        return photourl;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCameraMaker() {
        return cameraMaker;
    }

    public void setCameraMaker(String cameraMaker) {
        this.cameraMaker = cameraMaker;
    }

    public String getCameraModel() {
        return cameraModel;
    }

    public void setCameraModel(String cameraModel) {
        this.cameraModel = cameraModel;
    }

    public String getFstop() {
        return fstop;
    }

    public void setFstop(String fstop) {
        this.fstop = fstop;
    }

    public String getExposureTime() {
        return exposureTime;
    }

    public void setExposureTime(String exposureTime) {
        this.exposureTime = exposureTime;
    }

    public String getIsospeed() {
        return isospeed;
    }

    public void setIsospeed(String isospeed) {
        this.isospeed = isospeed;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Float getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Float totalRating) {
        this.totalRating = totalRating;
    }

    public Float getCurrentUserRating() {
        return currentUserRating;
    }

    public void setCurrentUserRating(Float currentUserRating) {
        this.currentUserRating = currentUserRating;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Komentar> getKomentars() {
        return komentars;
    }

    public void setKomentars(List<Komentar> komentars) {
        this.komentars = komentars;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

}