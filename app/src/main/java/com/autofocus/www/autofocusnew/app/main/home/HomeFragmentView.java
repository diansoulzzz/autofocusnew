package com.autofocus.www.autofocusnew.app.main.home;

import com.autofocus.www.autofocusnew.models.PostingResponse;

/**
 * Created by GBS on 18/01/2018.
 */

public interface HomeFragmentView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getPostingList(PostingResponse response);
}
