package com.autofocus.www.autofocusnew.app.posting;

public interface PostingView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);
}
