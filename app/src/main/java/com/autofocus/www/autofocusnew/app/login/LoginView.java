package com.autofocus.www.autofocusnew.app.login;


/**
 * Created by GBS on 17/01/2018.
 */

public interface LoginView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void startNextActivity();

    void showEmailError(CharSequence error);

    void showPasswordError(CharSequence error);
}
