package com.autofocus.www.autofocusnew.app.comment;

import com.autofocus.www.autofocusnew.models.PostingDataResponse;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;

public interface CommentView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void startNextActivity();

    void setPostingCaption(String caption);

    void setPostingImage(RequestOptions requestOptions, RequestListener listener, String imageurl);

    void setProfileImage(RequestOptions requestOptions, String imageurl);

    void setCommentText(CharSequence text);

    void scrollToBottom();

    void showNameError(CharSequence error);

    void showEmailError(CharSequence error);

    void showPasswordError(CharSequence error);

    void setPostingSpect(String cmrMaker, String cmrModel, String cmrExposure, String cmrFstop, String cmrIso, String cmrLocation, String cmrLat, String cmrLng);

    void getPostingWithComment(PostingDataResponse response);
}
