package com.autofocus.www.autofocusnew.app.main.search.users;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBaseFragment;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.login.LoginActivity;
import com.autofocus.www.autofocusnew.models.Posting;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.models.User;
import com.autofocus.www.autofocusnew.models.UserResponse;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class SearchUserFragment extends AppBaseFragment implements SearchUserFragmentView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

    @Inject
    public Service service;

    View rootView;
    SearchUserFragmentPresenter presenter;
    SharedPreferences preferences;

    public SearchUserFragment() {
    }

    public static SearchUserFragment newInstance() {
        SearchUserFragment fragment = new SearchUserFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search_user, container, false);
        ButterKnife.bind(this, rootView);
        preferences = getContext().getSharedPreferences("prefs", MODE_PRIVATE);
        presenter = new SearchUserFragmentPresenter(service, this, preferences);
        presenter.getUserList();
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getUserList();
                swiperefresh.setRefreshing(false);
            }
        });
        list.setLayoutManager(new LinearLayoutManager(this.getContext()));
        return rootView;
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void getUserList(UserResponse response) {
        SearchUserFragmentAdapter adapter = new SearchUserFragmentAdapter(this.getContext(), response.getData(),
                new SearchUserFragmentAdapter.OnBtnFollowClickListener() {
                    @Override
                    public void onClick(final User Item) {
                        if (Item.getFollowed()==1)
                        {
                            new AlertDialog.Builder(getContext())
                                .setTitle(R.string.id_menu_unfollow)
                                .setMessage(getString(R.string.id_confirm_unfollow, Item.getName()))
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        presenter.toogleFollow(Item.getId());
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(getContext(), R.string.id_confirm_cancel, Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .show();
                        }
                        else {
                            presenter.toogleFollow(Item.getId());
                            Toast.makeText(getContext(), getString(R.string.id_confirm_follow, Item.getName()), Toast.LENGTH_SHORT).show();
                        }


                    }
                }
        );
        list.setAdapter(adapter);

    }
}
