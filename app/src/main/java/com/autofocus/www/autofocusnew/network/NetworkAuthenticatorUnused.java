package com.autofocus.www.autofocusnew.network;

import android.support.annotation.Nullable;

import com.autofocus.www.autofocusnew.utils.TokenManager;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class NetworkAuthenticatorUnused implements Authenticator {

    private TokenManager tokenManager;
    private static NetworkAuthenticatorUnused INSTANCE;

    private NetworkAuthenticatorUnused(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    static synchronized NetworkAuthenticatorUnused getInstance(TokenManager tokenManager) {
        if (INSTANCE == null) {
            INSTANCE = new NetworkAuthenticatorUnused(tokenManager);
        }
        return INSTANCE;
    }


//    @Nullable
//    @Override
//    public Request authenticate(Route route, Response response) throws IOException {
//        if (responseCount(response) >= 3) {
//            return null;
//        }
//        AccessToken token = tokenManager.getToken();
//        NetworkService service = RetrofitBuilder.createService(NetworkService.class);
//        Observable<AccessToken> call = service.refresh(token.getRefreshToken() + "a");
//        retrofit2.Response<AccessToken> res = call.execute();
//        if (res.isSuccessful()) {
//            AccessToken newToken = res.body();
//            tokenManager.saveToken(newToken);
//            return response.request().newBuilder().header("Authorization", "Bearer " + res.body().getAccessToken()).build();
//        } else {
//            return null;
//        }
//    }

    private int responseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }

    @Nullable
    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        return null;
    }
}