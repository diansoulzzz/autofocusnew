package com.autofocus.www.autofocusnew.app.comment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.models.Komentar;
import com.autofocus.www.autofocusnew.models.Posting;
import com.autofocus.www.autofocusnew.models.PostingDataResponse;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.network.Service;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentActivity extends AppBase implements CommentView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.tvPostingCaption)
    TextView tvPostingCaption;
    @BindView(R.id.imgPosting)
    ImageView imgPosting;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.txtComment)
    TextView txtComment;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.nestScroll)
    NestedScrollView nestedScroll;
    @BindView(R.id.txtCameraMaker)
    EditText txtCameraMaker;
    @BindView(R.id.txtCameraModel)
    EditText txtCameraModel;
    @BindView(R.id.txtCameraExposure)
    EditText txtCameraExposure;
    @BindView(R.id.txtCameraFstop)
    EditText txtCameraFstop;
    @BindView(R.id.txtCameraIso)
    EditText txtCameraIso;
    @BindView(R.id.txtLocation)
    EditText txtLocation;
    @BindView(R.id.btnShowOnGoogleMap)
    Button btnShowOnGoogleMap;
    @Inject
    public Service service;
    CommentPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_comment);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        Integer posting_id = getIntent().getExtras().getInt("posting_id");
        ButterKnife.bind(this);
        list.setLayoutManager(new LinearLayoutManager(this));
//        list.setNestedScrollingEnabled(false);
        presenter = new CommentPresenter(service, this, preferences, posting_id);
        presenter.getPostingWithComment();
    }

    @OnClick(R.id.btnComment)
    void applyComment() {
        presenter.addComment(txtComment.getText().toString());
    }

    String lat,lng,loc;
    @OnClick(R.id.btnShowOnGoogleMap)
    void showOnGoogleMap(){
//        Uri gmmIntentUri = Uri.parse("geo:"+lat+","+lng+"(Google+Sydney)");
        Uri gmmIntentUri = Uri.parse("geo:"+lat+","+lng+"?q="+lat+","+lng+"("+loc+")");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(getApplicationContext(), appErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void startNextActivity() {

    }

    @Override
    public void setPostingCaption(String caption) {
        tvPostingCaption.setText(caption);
    }

    @Override
    public void setPostingImage(RequestOptions requestOptions, RequestListener listener, String imageurl) {
        Glide.with(getApplicationContext())
                .load(imageurl)
                .apply(requestOptions)
                .listener(listener)
                .into(imgPosting);
    }

    @Override
    public void setProfileImage(RequestOptions requestOptions, String imageurl) {
        Glide.with(getApplicationContext())
                .load(imageurl)
                .apply(requestOptions)
                .into(imgProfile);
    }

    @Override
    public void setCommentText(CharSequence text) {
        txtComment.setText(text);
    }

    @Override
    public void scrollToBottom() {
        nestedScroll.fullScroll(View.FOCUS_DOWN);
    }

    @Override
    public void showNameError(CharSequence error) {

    }

    @Override
    public void showEmailError(CharSequence error) {

    }

    @Override
    public void showPasswordError(CharSequence error) {

    }


    @Override
    public void setPostingSpect(String cmrMaker, String cmrModel, String cmrExposure, String cmrFstop, String cmrIso, String cmrLocation,  String cmrLat, String cmrLng) {
        txtCameraMaker.setText(cmrMaker);
        txtCameraModel.setText(cmrModel);
        txtCameraExposure.setText(cmrExposure);
        txtCameraFstop.setText(cmrFstop);
        txtCameraIso.setText(cmrIso);
        txtLocation.setText(cmrLocation);
        if (!TextUtils.isEmpty(cmrLocation)){
            lat=cmrLat;
            lng=cmrLng;
            loc=cmrLocation;
            btnShowOnGoogleMap.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getPostingWithComment(PostingDataResponse response) {
        CommentAdapter adapter = new CommentAdapter(getApplicationContext(), response.getData().getKomentars(),
                new CommentAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(Komentar Item) {
                        Toast.makeText(getApplicationContext(), Item.getIsiKomentar(),
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        list.setAdapter(adapter);
    }
}
