package com.autofocus.www.autofocusnew.app.posting.camera;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.models.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostingCameraFragmentAdapter extends RecyclerView.Adapter<PostingCameraFragmentAdapter.ViewHolder> {
    private final OnBtnFollowClickListener btnFollowClickListener;
    private List<User> data;
    private Context context;

    public PostingCameraFragmentAdapter(Context context, List<User> data, OnBtnFollowClickListener btnFollowClickListener) {
        this.data = data;
        this.btnFollowClickListener = btnFollowClickListener;
        this.context = context;
    }

    @Override
    public PostingCameraFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_user, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(PostingCameraFragmentAdapter.ViewHolder holder, int position) {
        //init event
        holder.btnFollowclick(data.get(position), btnFollowClickListener);
        holder.tvProfileName.setText(data.get(position).getName());
        String profileImage = data.get(position).getPhotourl();
        if (data.get(position).getFollowed()==1)
        {
            holder.btnFollow.setBackgroundColor(Color.GRAY);
            holder.btnFollow.setText(R.string.id_followed);
        }
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        requestOptions.skipMemoryCache(true);
        Glide.with(context)
                .load(profileImage)
                .apply(requestOptions)
                .into(holder.imgProfile);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public interface OnBtnFollowClickListener {
        void onClick(User Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.tvProfileName)
        TextView tvProfileName;
        @Nullable
        @BindView(R.id.imgProfile)
        ImageView imgProfile;
        @Nullable
        @BindView(R.id.rabStars)
        RatingBar rabStars;
        @Nullable
        @BindView(R.id.btnFollow)
        Button btnFollow;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void btnFollowclick(final User data, final OnBtnFollowClickListener listener) {
            btnFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(data);
                }
            });
        }
    }


}