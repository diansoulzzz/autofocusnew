package com.autofocus.www.autofocusnew.app.main.search.maps;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.models.Posting;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

class SearchMapFragmentAdapter implements GoogleMap.InfoWindowAdapter,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener {
    @Nullable
    @BindView(R.id.imgPosting)
    ImageView imgPosting;
    @Nullable
    @BindView(R.id.tvPostingCaption)
    TextView tvPostingCaption;
    @Nullable
    @BindView(R.id.tvPostingLocation)
    TextView tvPostingLocation;

    private final View mWindow;
    //    private final View mContents;
    private List<Posting> data;
    private Context context;
    private final onMapAdapterEventListener listener;
    private SearchMapFragmentView searchMapFragmentView;
    private final String TAG = "MAP";

    SearchMapFragmentAdapter(Context context, List<Posting> data, onMapAdapterEventListener listener, SearchMapFragmentView searchMapFragmentView) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mWindow = inflater.inflate(R.layout.item_map_window, null);
        ButterKnife.bind(this, mWindow);
        this.context = context;
        this.data = data;
        this.listener = listener;
        this.searchMapFragmentView = searchMapFragmentView;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        Log.d(TAG, TAG + "window"+marker.isInfoWindowShown());
//        onProsessRender(tvPostingCaption,tvPostingLocation,imgPosting,marker,onRenderable);
        return mWindow;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        Log.d(TAG, "contex");
        return mWindow;
    }

    boolean notshow;
    @Override
    public boolean onMarkerClick(final Marker marker) {
        int position = Integer.valueOf(marker.getTag().toString());
        notshow = true;
        searchMapFragmentView.showWait();
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .override(200, 200)
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.drawable.ic_search_black_24dp);
        Glide.with(context)
                .load(data.get(position).getPhotourl())
                .apply(requestOptions)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        searchMapFragmentView.removeWait();
                        notshow = false;
                        return false;
                    }
                })
                .into(imgPosting);
        return notshow;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        int position = Integer.valueOf(marker.getTag().toString());
        onMapInfoWindowClickListener(data.get(position),listener);
    }

//    private void render(final Marker marker, final View mWindow) {
//        String imageurl = marker.getTag().toString();
//        RequestOptions requestOptions = new RequestOptions()
//                .centerCrop()
//                .override(200, 200)
//                .placeholder(R.mipmap.ic_launcher_round)
//                .error(R.drawable.ic_search_black_24dp);
//        Glide.with(context)
//                .load(marker.getTag())
//                .apply(requestOptions)
//                .listener(new RequestListener<Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                        tvPostingCaption.setText(marker.getTitle());
//                        tvPostingLocation.setText(marker.getSnippet());
//                        return false;
//                    }
//                })
//                .into(imgPosting);
//
//    }

    public interface onMapAdapterEventListener {
        void onMapInfoWindowClick(Posting posting);
    }

    public void onMapInfoWindowClickListener(Posting item, final onMapAdapterEventListener listener)
    {
        listener.onMapInfoWindowClick(item);
    }
}
