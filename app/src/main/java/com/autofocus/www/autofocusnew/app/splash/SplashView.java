package com.autofocus.www.autofocusnew.app.splash;

/**
 * Created by GBS on 17/01/2018.
 */

public interface SplashView {
    void startNextActivity();
}
