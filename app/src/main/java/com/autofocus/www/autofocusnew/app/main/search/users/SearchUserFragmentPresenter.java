package com.autofocus.www.autofocusnew.app.main.search.users;

import android.content.SharedPreferences;
import android.util.Log;

import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.models.UserResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class SearchUserFragmentPresenter {
    private final Service service;
    private final SearchUserFragmentView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "SearchUserPresenter";
    private TokenManager tokenManager;

    public SearchUserFragmentPresenter(Service service, SearchUserFragmentView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }
    public void getUserList() {
        view.showWait();
        Subscription subscription = service.getUserList(new Service.GetUserListCallback() {
            @Override
            public void onSuccess(UserResponse response) {
                view.removeWait();
                view.getUserList(response);
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG,errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
            }

        },tokenManager.getToken().getAccessToken());
        subscriptions.add(subscription);
    }
    public void toogleFollow(Integer users_id)
    {
        view.showWait();
        Subscription subscription = service.toogleFollowGetUserList(new Service.GetUserListCallback() {
            @Override
            public void onSuccess(UserResponse response) {
                view.removeWait();
                view.getUserList(response);
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG,errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
            }

        },tokenManager.getToken().getAccessToken(),users_id);
        subscriptions.add(subscription);
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }
}
