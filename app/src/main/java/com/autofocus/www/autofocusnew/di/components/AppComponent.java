package com.autofocus.www.autofocusnew.di.components;

import com.autofocus.www.autofocusnew.app.changepassword.ChangePasswordActivity;
import com.autofocus.www.autofocusnew.app.changeprofile.ChangeProfileActivity;
import com.autofocus.www.autofocusnew.app.comment.CommentActivity;
import com.autofocus.www.autofocusnew.app.login.LoginActivity;
import com.autofocus.www.autofocusnew.app.main.MainActivity;
import com.autofocus.www.autofocusnew.app.main.home.HomeFragment;
import com.autofocus.www.autofocusnew.app.main.profile.ProfileFragment;
import com.autofocus.www.autofocusnew.app.main.search.SearchFragment;
import com.autofocus.www.autofocusnew.app.main.search.maps.SearchMapFragment;
import com.autofocus.www.autofocusnew.app.main.search.users.SearchUserFragment;
import com.autofocus.www.autofocusnew.app.posting.PostingActivity;
import com.autofocus.www.autofocusnew.app.posting.camera.PostingCameraFragment;
import com.autofocus.www.autofocusnew.app.posting.gallery.PostingGalleryFragment;
import com.autofocus.www.autofocusnew.app.preview.PreviewActivity;
import com.autofocus.www.autofocusnew.app.register.RegisterActivity;
import com.autofocus.www.autofocusnew.app.upload.UploadActivity;
import com.autofocus.www.autofocusnew.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class,})
public interface AppComponent {
    void inject(LoginActivity activity);
    void inject(RegisterActivity activity);
    void inject(MainActivity activity);
    void inject(CommentActivity activity);
    void inject(HomeFragment fragment);
    void inject(SearchFragment fragment);
    void inject(SearchUserFragment fragment);
    void inject(SearchMapFragment fragment);
    void inject(ProfileFragment fragment);
    void inject(PostingActivity activity);
    void inject(PostingGalleryFragment fragment);
    void inject(PostingCameraFragment fragment);
    void inject(PreviewActivity activity);
    void inject(UploadActivity activity);
    void inject(ChangeProfileActivity activity);
    void inject(ChangePasswordActivity activity);
}