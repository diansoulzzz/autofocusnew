package com.autofocus.www.autofocusnew;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.autofocus.www.autofocusnew.di.components.AppComponent;
import com.autofocus.www.autofocusnew.di.components.DaggerAppComponent;
import com.autofocus.www.autofocusnew.network.NetworkModule;
import com.google.firebase.storage.FirebaseStorage;

import java.io.File;

public class AppBaseFragment extends Fragment {


    AppComponent daggerInterface;
    private static Context context = null;
    private static Activity activity = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getActivity().getApplicationContext();
        activity = this.getActivity();
        File cacheFile = new File(context.getCacheDir(), "responses");
        daggerInterface = DaggerAppComponent.builder().networkModule(new NetworkModule(cacheFile)).build();
    }

    public AppComponent getAppComponent() {
        return daggerInterface;
    }

    public Activity getFragmentActivity() {
        return activity;
    }

    public static Context getFragmentContext()
    {
        return context;
    }


    public interface Callback {
        void onFragmentAttached();
        void onFragmentDetached(String tag);
    }

}
