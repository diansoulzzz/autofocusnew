package com.autofocus.www.autofocusnew.app.splash;

import android.content.Intent;
import android.os.Bundle;

import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.app.login.LoginActivity;

public class SplashActivity extends AppBase implements SplashView {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SplashPresenter presenter = new SplashPresenter(this);
        presenter.startNextActivity();
    }

    @Override
    public void startNextActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}