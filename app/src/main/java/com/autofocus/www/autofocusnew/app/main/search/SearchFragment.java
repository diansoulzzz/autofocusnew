package com.autofocus.www.autofocusnew.app.main.search;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBaseFragment;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.main.search.maps.SearchMapFragment;
import com.autofocus.www.autofocusnew.app.main.search.users.SearchUserFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Dian Yulius on 1/18/2018.
 */

public class SearchFragment extends AppBaseFragment {

    private View rootView;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    @Nullable
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @Nullable
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;


    public SearchFragment() {
    }

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search, container, false);

        ButterKnife.bind(this, rootView);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(mSectionsPagerAdapter);
        setHasOptionsMenu(true);
        tabLayout.setupWithViewPager(viewPager);
        return rootView;

    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        AppBarLayout appBarLayout = getActivity().findViewById(R.id.appBarLayout);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            appBarLayout.setElevation(0);
//        }
//        super.onActivityCreated(savedInstanceState);
//    }

    float elevation = 0;
    @Override
    public void onStart() {
//        Toast.makeText(getContext(),"start",Toast.LENGTH_SHORT).show();
        AppBarLayout appBarLayout = getActivity().findViewById(R.id.appBarLayout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            elevation = appBarLayout.getElevation();
            appBarLayout.setElevation(0);
        }
        super.onStart();
    }

    @Override
    public void onDestroy() {
//        Toast.makeText(getContext(),"destroy",Toast.LENGTH_SHORT).show();
        AppBarLayout appBarLayout = getActivity().findViewById(R.id.appBarLayout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            appBarLayout.setElevation(elevation);
        }
        super.onDestroy();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return SearchUserFragment.newInstance();
                case 1:
                    return SearchMapFragment.newInstance();
            }
            return SearchUserFragment.newInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "USER";
                case 1:
                    return "MAPS";
            }
            return null;
        }
    }
}
