package com.autofocus.www.autofocusnew.app.main.search.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBaseFragment;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.comment.CommentActivity;
import com.autofocus.www.autofocusnew.models.Posting;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.OnMapAndViewReadyListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class SearchMapFragment extends AppBaseFragment implements SearchMapFragmentView,
        GoogleMap.OnMarkerDragListener,
        SeekBar.OnSeekBarChangeListener,
        GoogleMap.OnInfoWindowLongClickListener,
        GoogleMap.OnInfoWindowCloseListener,
        OnMapAndViewReadyListener.OnGlobalLayoutAndMapReadyListener {

    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.container)
    RelativeLayout container;

    @Inject
    public Service service;

    View rootView;
    SearchMapFragmentPresenter presenter;
    SharedPreferences preferences;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    private final String TAG = "MAP";
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final float maxZoomLevel = 16.0f;
    public SearchMapFragment() {
    }

    public static SearchMapFragment newInstance() {
        SearchMapFragment fragment = new SearchMapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }

    private void showSnackbar(final String text) {
        Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(container, getString(mainTextStringId), Snackbar.LENGTH_INDEFINITE).setAction(getString(actionStringId), listener).show();
    }

    private void requestPermissions() {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.id_permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startLocationPermissionRequest();
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            startLocationPermissionRequest();
        }
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search_map, container, false);
        ButterKnife.bind(this, rootView);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        new OnMapAndViewReadyListener(mapFragment, this); // check map ready?
        preferences = getContext().getSharedPreferences("prefs", MODE_PRIVATE);
        presenter = new SearchMapFragmentPresenter(service, this, preferences);
        presenter.getPostingList();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        return rootView;
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, maxZoomLevel));
                        } else {
                            Log.w("MAP", "getLastLocation:exception", task.getException());
                            showSnackbar(getString(R.string.id_no_location_detected));
                        }
                    }
                });
    }

    @SuppressLint("MissingPermission")
    @Override
    public void getPostingList(final PostingResponse response, final RequestOptions requestOptions) {
        Log.d(TAG, "POSTING");
        SearchMapFragmentAdapter adapter = new SearchMapFragmentAdapter(this.getContext(), response.getData(), new SearchMapFragmentAdapter.onMapAdapterEventListener() {
            @Override
            public void onMapInfoWindowClick(Posting Item) {
                Intent intent = new Intent(getContext(), CommentActivity.class);
                intent.putExtra("posting_id", Item.getId());
                startActivity(intent);
            }
        }, this);
        mMap.setInfoWindowAdapter(adapter);
        mMap.setOnMarkerClickListener(adapter);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setMaxZoomPreference(maxZoomLevel);
        mMap.setOnInfoWindowClickListener(adapter);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnInfoWindowCloseListener(this);
        mMap.setOnInfoWindowLongClickListener(this);
        mMap.setContentDescription("Map with lots of markers.");
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void downloadAllImage(Posting posting, RequestOptions requestOptions) {
        FutureTarget<File> future = Glide.with(this.getContext())
                .load(posting.getPhotourl())
                .downloadOnly(200, 200);
        Glide.with(this.getContext())
                .load(posting.getPhotourl())
                .preload(200, 200);
    }

    @Override
    public void addMapMarker(Posting posting, Integer index) {
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(posting.getLat(), posting.getLng()))
                .title(posting.getCaption())
                .snippet(posting.getLocation())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        );
        marker.setTag(index);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "Ready");
        mMap = googleMap;
    }

    @Override
    public void onInfoWindowClose(Marker marker) {

    }

    @Override
    public void onInfoWindowLongClick(Marker marker) {
        Log.d(TAG, "once");
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }
}
