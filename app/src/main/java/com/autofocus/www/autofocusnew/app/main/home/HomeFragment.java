package com.autofocus.www.autofocusnew.app.main.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBaseFragment;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.comment.CommentActivity;
import com.autofocus.www.autofocusnew.app.main.MainActivity;
import com.autofocus.www.autofocusnew.app.main.MainView;
import com.autofocus.www.autofocusnew.models.Posting;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.network.Service;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends AppBaseFragment implements HomeFragmentView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

    @Inject
    public Service service;

    View rootView;
    HomeFragmentPresenter presenter;
    SharedPreferences preferences;

    private MainView listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (MainView) context;
        } catch (ClassCastException castException) {
            /* The activity does not implement the listener. */
        }
    }
    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);
        preferences = getContext().getSharedPreferences("prefs", MODE_PRIVATE);
        list.setLayoutManager(new LinearLayoutManager(this.getContext()));
        presenter = new HomeFragmentPresenter(service, this, preferences);
        presenter.getPostingList();

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getPostingList();
                swiperefresh.setRefreshing(false);
            }
        });
        return rootView;
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
        getFragmentActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
        getFragmentActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(getContext(), appErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void getPostingList(PostingResponse response) {
        HomeFragmentAdapter adapter2 = new HomeFragmentAdapter(getContext(), response.getData(), new HomeFragmentAdapter.OnItemClickListener() {
            @Override
            public void onClick(Posting Item) {
                Toast.makeText(getContext(), Item.getCaption(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStarsChoose(Posting Item, Float rating, HomeFragmentAdapter.ViewHolderPosting holder) {
                presenter.setRating(Item.getId(), rating, holder);
            }
        }, new HomeFragmentAdapter.OnBtnCommentClickListener() {
            @Override
            public void onClick(Posting Item) {
                Intent intent = new Intent(getContext(), CommentActivity.class);
                intent.putExtra("posting_id", Item.getId());
                startActivity(intent);
            }
        }, new HomeFragmentAdapter.OnBtnFollowClickListener() {
            @Override
            public void onClick() {
                listener.setBottomMenuIndex(R.id.navigation_search);
            }
        }, new HomeFragmentAdapter.OnBtnPostingClickListener() {
            @Override
            public void onClick() {
                listener.setBottomMenuIndex(R.id.navigation_posting);
            }
        });
//        HomeFragmentAdapter adapter = new HomeFragmentAdapter(this.getContext(), response.getData(),
//                new HomeFragmentAdapter.OnItemClickListener() {
//                    @Override
//                    public void onClick(Posting Item) {
////                        Toast.makeText(getContext(), Item.getCaption(),Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void onStarsChoose(Posting Item, Float rating, HomeFragmentAdapter.ViewHolder holder) {
//                        presenter.setRating(Item.getId(), rating, holder);
//                    }
//
//                },
//                new HomeFragmentAdapterOld.OnBtnCommentClickListener() {
//                    @Override
//                    public void onClick(Posting Item) {
////                        Toast.makeText(getContext(), Item.getCaption(), Toast.LENGTH_LONG).show();
//                        Intent intent = new Intent(getContext(), CommentActivity.class);
//                        intent.putExtra("posting_id", Item.getId());
//                        startActivity(intent);
//                    }
//                }
//        );

        list.setAdapter(adapter2);

    }
}
