package com.autofocus.www.autofocusnew.app.main.search.maps;

import com.autofocus.www.autofocusnew.models.Posting;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.bumptech.glide.request.RequestOptions;

public interface SearchMapFragmentView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getPostingList(PostingResponse response, RequestOptions requestOptions);

    void addMapMarker(Posting posting, Integer index);

    void downloadAllImage(Posting posting, RequestOptions requestOptions);
}
