package com.autofocus.www.autofocusnew.app.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.main.home.HomeFragment;
import com.autofocus.www.autofocusnew.app.main.profile.ProfileFragment;
import com.autofocus.www.autofocusnew.app.main.search.SearchFragment;
import com.autofocus.www.autofocusnew.app.posting.PostingActivity;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.BottomNavigationViewHelper;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppBase implements MainView {
    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    @Inject
    public Service service;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    MainPresenter presenter;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getAppComponent().inject(this);
        ButterKnife.bind(this);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        presenter = new MainPresenter(service, this, preferences);
        setSupportActionBar(toolBar);
        init();
    }

    public void init() {
        BottomNavigationViewHelper.disableShiftMode(navigation);
        fragmentManager = getSupportFragmentManager();
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == navigation.getSelectedItemId()) return false;
                transaction = fragmentManager.beginTransaction();
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        transaction.replace(R.id.content, new HomeFragment()).commit();
                        return true;
                    case R.id.navigation_search:
                        transaction.replace(R.id.content, new SearchFragment()).commit();
                        return true;
                    case R.id.navigation_posting:
                        startActivity(new Intent(MainActivity.this, PostingActivity.class));
                        return false;
                    case R.id.navigation_profile:
                        transaction.replace(R.id.content, new ProfileFragment()).commit();
                        return true;
                }
                return false;
            }
        });
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content, new HomeFragment()).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setBottomMenuIndex(Integer index) {
        navigation.setSelectedItemId(index);
    }


    @Override
    public void showWait() {

    }

    @Override
    public void removeWait() {

    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

}
