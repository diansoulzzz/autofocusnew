package com.autofocus.www.autofocusnew.app.upload;

import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;

public interface UploadView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void clearActivity();

    void setPreviewImage(String path);

    void setProfileImage(RequestOptions requestOptions, RequestListener listener, String imageurl);

    void setCaptionText(String text);

    void showCaptionError(CharSequence text);

    void setCameraModelText(String text);

    void showCameraModelError(CharSequence text);

    void setCameraMakerText(String text);

    void showCameraMakerError(CharSequence text);

    void setCameraExposureText(String text);

    void showCameraExposureError(CharSequence text);

    void setCameraFstopText(String text);

    void showCameraFstopError(CharSequence text);

    void setCameraIsoText(String text);

    void showCameraIsoError(CharSequence text);

    void setLocationText(String text);
}
