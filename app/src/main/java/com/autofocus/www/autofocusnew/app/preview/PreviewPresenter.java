package com.autofocus.www.autofocusnew.app.preview;

import android.content.SharedPreferences;

import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import rx.subscriptions.CompositeSubscription;

public class PreviewPresenter {
    private final Service service;
    private final PreviewView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "UploadPresenter";
    private TokenManager tokenManager;

    public PreviewPresenter(Service service, PreviewView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

}
