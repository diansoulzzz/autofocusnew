package com.autofocus.www.autofocusnew.app.posting.gallery;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.autofocus.www.autofocusnew.AppBaseFragment;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.preview.PreviewActivity;
import com.autofocus.www.autofocusnew.network.Service;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class PostingGalleryFragment extends AppBaseFragment implements PostingGalleryFragmentView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.list)
    RecyclerView list;
    @Inject
    public Service service;
    View rootView;
    PostingGalleryFragmentPresenter presenter;
    SharedPreferences preferences;

    final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED};


    public PostingGalleryFragment() {
    }

    public static PostingGalleryFragment newInstance() {
        PostingGalleryFragment fragment = new PostingGalleryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_posting_gallery, container, false);
        ButterKnife.bind(this, rootView);
        preferences = getContext().getSharedPreferences("prefs", MODE_PRIVATE);
        presenter = new PostingGalleryFragmentPresenter(service, this, preferences);
        int numberOfColumns = 3;
        list.setLayoutManager(new GridLayoutManager(this.getContext(), numberOfColumns));
        final Cursor cursor = getContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, MediaStore.Images.Media.DATE_ADDED + " DESC");
        final List<String> imagePaths = presenter.getImagePaths(cursor);
        PostingGalleryFragmentAdapter adapter = new PostingGalleryFragmentAdapter(this.getContext(), imagePaths, new PostingGalleryFragmentAdapter.OnItemClickListener() {
            @Override
            public void onClick(String Item) {
                startNextActivity(Item);
            }
        });
        list.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void startNextActivity(String path) {
        Intent intent = new Intent(rootView.getContext(), PreviewActivity.class);
        intent.putExtra("imagefilepath", path);
        startActivity(intent);
    }
}
