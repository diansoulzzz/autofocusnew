package com.autofocus.www.autofocusnew.network;

import android.text.TextUtils;
import android.util.Log;

import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import retrofit2.adapter.rxjava.HttpException;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class NetworkError extends Throwable {
    public static final String DEFAULT_ERROR_MESSAGE = "Something went wrong! Please try again.";
    public static final String NETWORK_ERROR_MESSAGE = "No Internet Connection!";
    private static final String ERROR_MESSAGE_HEADER = "Error-Message";
    public static final String TAG = "NetworkError";
    private final Throwable error;

    public NetworkError(Throwable e) {
        super(e);
        this.error = e;
    }

    public String getMessage() {
        return error.getMessage();
    }

    public boolean isAuthFailure() {
        return error instanceof HttpException &&
                ((HttpException) error).code() == HTTP_UNAUTHORIZED;
    }

    public boolean isResponseNull() {
        return error instanceof HttpException && ((HttpException) error).response() == null;
    }

//    public String getAppErrorMessage() {
//        if (this.error instanceof IOException) return NETWORK_ERROR_MESSAGE;
//        if (!(this.error instanceof HttpException)) return DEFAULT_ERROR_MESSAGE;
//        retrofit2.Response<?> response = ((HttpException) this.error).response();
//        if (response != null) {
//            ErrorResponse errorResponse = getJsonObjectFromResponse(response);
//            if (!TextUtils.isEmpty(errorResponse.getMessage())) return errorResponse.getMessage();
//            Map<String, List<String>> headers = response.headers().toMultimap();
//            if (headers.containsKey(ERROR_MESSAGE_HEADER))
//                return headers.get(ERROR_MESSAGE_HEADER).get(0);
//        }
//        return DEFAULT_ERROR_MESSAGE;
//    }

    public ErrorResponse getErrorResponse()
    {
        ErrorResponse errorResponse = new ErrorResponse();
        if (this.error instanceof IOException)
        {
            errorResponse.setMessage(NETWORK_ERROR_MESSAGE);
            return errorResponse;
        }
        if (!(this.error instanceof HttpException))
        {
            errorResponse.setMessage(DEFAULT_ERROR_MESSAGE);
            return errorResponse;
        }
        retrofit2.Response<?> response = ((HttpException) this.error).response();
        if (response != null) {
            errorResponse = getJsonObjectFromResponse(response);
        }
        return errorResponse;
    }

    protected ErrorResponse getJsonObjectFromResponse(final retrofit2.Response<?> response) {
        try {
            String jsonString = response.errorBody().string();
            ErrorResponse errorResponse = null;
            try
            {
                errorResponse = new Gson().fromJson(jsonString, ErrorResponse.class);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
            return errorResponse;
        } catch (Exception e) {
            return null;
        }
    }

    public Throwable getError() {
        return error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NetworkError that = (NetworkError) o;

        return error != null ? error.equals(that.error) : that.error == null;

    }

    @Override
    public int hashCode() {
        return error != null ? error.hashCode() : 0;
    }
}