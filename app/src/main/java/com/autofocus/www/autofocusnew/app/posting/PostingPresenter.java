package com.autofocus.www.autofocusnew.app.posting;

import android.content.SharedPreferences;
import android.util.Log;

import com.autofocus.www.autofocusnew.models.AccessToken;
import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import java.util.List;
import java.util.Map;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class PostingPresenter {
    private final Service service;
    private final PostingView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "UploadPresenter";
    private TokenManager tokenManager;

    public PostingPresenter(Service service, PostingView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

}
