package com.autofocus.www.autofocusnew.app.preview;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.media.ExifInterface;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.upload.UploadActivity;
import com.autofocus.www.autofocusnew.network.Service;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

public class PreviewActivity extends AppBase implements PreviewView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.imgPreview)
    ImageView imgPreview;

    @Inject
    public Service service;
    PreviewPresenter presenter;
    private static final String TAG = "UploadActivity";
    private String filepath;
    private static ExifInterface ei;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_preview);
        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        filepath = intent.getStringExtra("imagefilepath");
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        presenter = new PreviewPresenter(service, this, preferences);
        try {
            Bitmap image = handleSamplingAndRotationBitmap(getContext(),Uri.fromFile(new File(filepath)));
            imgPreview.setImageBitmap(image);
            File files = saveImage(image);
            ExifInterface exif = new ExifInterface(files.getCanonicalPath());
            exif.setAttribute(ExifInterface.TAG_MAKE,ei.getAttribute(ExifInterface.TAG_MAKE));
            exif.setAttribute(ExifInterface.TAG_MODEL,ei.getAttribute(ExifInterface.TAG_MODEL));
            exif.setAttribute(ExifInterface.TAG_ISO_SPEED_RATINGS,ei.getAttribute(ExifInterface.TAG_ISO_SPEED_RATINGS));
            exif.setAttribute(ExifInterface.TAG_EXPOSURE_TIME,ei.getAttribute(ExifInterface.TAG_EXPOSURE_TIME));
            exif.setAttribute(ExifInterface.TAG_F_NUMBER,ei.getAttribute(ExifInterface.TAG_F_NUMBER));
            exif.saveAttributes();
            filepath = files.getPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        this.setPreviewImage(filepath);
    }

    public File saveImage(Bitmap finalBitmap)
    {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        String fname = "Image-Temp" + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {
        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }
    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);
        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void setPreviewImage(String path) {
        Glide.with(getContext()).load(new File(path)).apply(new RequestOptions().signature(new ObjectKey(String.valueOf(System.currentTimeMillis())))).into(imgPreview);
    }

    @Override
    public void startNextActivity() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.camera_preview, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_next:
                Intent intent = new Intent(this, UploadActivity.class);
                intent.putExtra("imagefilepath", filepath);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
