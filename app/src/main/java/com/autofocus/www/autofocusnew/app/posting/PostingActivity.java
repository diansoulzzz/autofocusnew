package com.autofocus.www.autofocusnew.app.posting;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.posting.camera.PostingCameraFragment;
import com.autofocus.www.autofocusnew.app.posting.gallery.PostingGalleryFragment;
import com.autofocus.www.autofocusnew.network.Service;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostingActivity extends AppBase implements PostingView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @Inject
    public Service service;
    PostingPresenter presenter;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static final String TAG = "UploadActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_posting);
        ButterKnife.bind(this);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        presenter = new PostingPresenter(service, this, preferences);
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "Items : " + String.valueOf(position));
            switch (position) {
                case 0:
                    return PostingGalleryFragment.newInstance();
                case 1:
                    return PostingCameraFragment.newInstance();
            }
            return PostingGalleryFragment.newInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "GALLERY";
                case 1:
                    return "CAMERA";
            }
            return null;
        }
    }
}
