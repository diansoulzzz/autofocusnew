package com.autofocus.www.autofocusnew.app.comment;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;

import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.PostingDataResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class CommentPresenter {
    private final Service service;
    private final CommentView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "ChangePasswordPresenter";
    private TokenManager tokenManager;
    private final Integer posting_id;

    public CommentPresenter(Service service, CommentView view, SharedPreferences preferences, Integer posting_id) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
        this.posting_id = posting_id;
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

    public void addComment(String comment) {
        Subscription subscription = service.addCommentGetPosting(new Service.GetPostingCallback() {
            @Override
            public void onSuccess(PostingDataResponse response) {
                Log.d(TAG, "onSuccess");
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                requestOptions.skipMemoryCache(true);
//                view.setPostingCaption(response.getData().getCaption());
//                view.setPostingImage(requestOptions, response.getData().getPhotourl());
//                view.setProfileImage(requestOptions, response.getData().getUser().getPhotourl());
                view.getPostingWithComment(response);
                view.setCommentText("");
                view.scrollToBottom();
                view.removeWait();
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
//                if (errorResponse.getErrors() != null)
//                {
//                    for (Map.Entry<String, List<String>> error : errorResponse.getErrors().entrySet()) {
//                        if (error.getKey().equals("name")) {
//                            view.showNameError(error.getValue().get(0));
//                        }
//                        if (error.getKey().equals("email")) {
//                            view.showEmailError(error.getValue().get(0));
//                        }
//                        if (error.getKey().equals("password")) {
//                            view.showPasswordError(error.getValue().get(0));
//                        }
//                    }
//                }
            }
        }, tokenManager.getToken().getAccessToken(), this.posting_id, comment);
        subscriptions.add(subscription);
        view.showWait();

    }

    public void getPostingWithComment() {
        view.showWait();
        Subscription subscription = service.getPostingWhereId(new Service.GetPostingCallback() {
            @Override
            public void onSuccess(PostingDataResponse response) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                requestOptions.skipMemoryCache(true);
                view.setPostingCaption(response.getData().getCaption());
                RequestListener listener = new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        view.removeWait();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        view.removeWait();
                        return false;
                    }
                };
                view.setPostingImage(requestOptions, listener, response.getData().getPhotourl());
                view.setProfileImage(requestOptions, response.getData().getUser().getPhotourl());
                view.setPostingSpect(response.getData().getCameraMaker(),response.getData().getCameraModel(),response.getData().getExposureTime(),response.getData().getFstop(),response.getData().getIsospeed(),response.getData().getLocation(),String.valueOf(response.getData().getLat()),String.valueOf(response.getData().getLng()));
                view.getPostingWithComment(response);
                Log.d(TAG, "onSuccess");
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
//                if (errorResponse.getErrors() != null)
//                {
//                    for (Map.Entry<String, List<String>> error : errorResponse.getErrors().entrySet()) {
//                        if (error.getKey().equals("name")) {
//                            view.showNameError(error.getValue().get(0));
//                        }
//                        if (error.getKey().equals("email")) {
//                            view.showEmailError(error.getValue().get(0));
//                        }
//                        if (error.getKey().equals("password")) {
//                            view.showPasswordError(error.getValue().get(0));
//                        }
//                    }
//                }
            }
        }, tokenManager.getToken().getAccessToken(), this.posting_id);
        subscriptions.add(subscription);
    }


}
