package com.autofocus.www.autofocusnew.app.upload;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.login.LoginActivity;
import com.autofocus.www.autofocusnew.app.main.MainActivity;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.CameraUtil;
import com.autofocus.www.autofocusnew.utils.TokenManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UploadActivity extends AppBase implements UploadView {
//    Menu menu;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.imgPreview)
    ImageView imgPreview;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.txtCaption)
    EditText txtCaption;
    @BindView(R.id.txtCameraMaker)
    EditText txtCameraMaker;
    @BindView(R.id.txtCameraModel)
    EditText txtCameraModel;
    @BindView(R.id.txtCameraExposure)
    EditText txtCameraExposure;
    @BindView(R.id.txtCameraFstop)
    EditText txtCameraFstop;
    @BindView(R.id.txtCameraIso)
    EditText txtCameraIso;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @Inject
    public Service service;
    UploadPresenter presenter;
    private static final String TAG = "UploadActivity";
    private static final int PLACE_PICKER_REQUEST = 1;
    double lat = 0, lng = 0;
    String filepath;
    Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_upload);
        ButterKnife.bind(this);
        activity = this;
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        filepath = intent.getStringExtra("imagefilepath");
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        presenter = new UploadPresenter(service, this, preferences, filepath, getFirebaseStorage());
        presenter.setAttribute();
        presenter.getUserProfile();

    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
//        MenuItem item = menu.findItem(R.id.menu_next);
//        item.setEnabled(false);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
//        MenuItem item = menu.findItem(R.id.menu_next);
//        item.setEnabled(true);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @OnClick(R.id.btnLocation)
    void addLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(activity), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(getContext(), appErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPreviewImage(String path) {
        Glide.with(getContext()).load(new File(path)).apply(new RequestOptions().signature(new ObjectKey(String.valueOf(System.currentTimeMillis())))).into(imgPreview);
    }

    @Override
    public void setProfileImage(RequestOptions requestOptions, RequestListener listener, String imageurl) {
        Glide.with(getApplicationContext()).load(imageurl).apply(requestOptions).listener(listener).into(imgProfile);
    }

    @Override
    public void setCaptionText(String text) {
        txtCaption.setText(text);
    }

    @Override
    public void showCaptionError(CharSequence text) {
        txtCaption.setError(text);
    }

    @Override
    public void setCameraModelText(String text) {
        txtCameraModel.setText(text);
    }

    @Override
    public void showCameraModelError(CharSequence text) {
        txtCameraModel.setError(text);
    }

    @Override
    public void setCameraMakerText(String text) {
        txtCameraMaker.setText(text);
    }

    @Override
    public void showCameraMakerError(CharSequence text) {
        txtCameraMaker.setError(text);
    }

    @Override
    public void setCameraExposureText(String text) {
        txtCameraExposure.setText(text);
    }

    @Override
    public void showCameraExposureError(CharSequence text) {
        txtCameraExposure.setError(text);
    }

    @Override
    public void setCameraFstopText(String text) {
        txtCameraFstop.setText(text);
    }

    @Override
    public void showCameraFstopError(CharSequence text) {
        txtCameraFstop.setError(text);
    }

    @Override
    public void setCameraIsoText(String text) {
        txtCameraIso.setText(text);
    }

    @Override
    public void showCameraIsoError(CharSequence text) {
        txtCameraIso.setError(text);
    }

    @Override
    public void setLocationText(String text) {
        tvLocation.setText(text);
    }

    @Override
    public void clearActivity() {
        CameraUtil.photoPostingClear(getContext());
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.camera_preview, menu);
//        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_next:
                StringBuilder strBuilder = new StringBuilder("");
                if (TextUtils.isEmpty(txtCaption.getText().toString())) {
                    strBuilder.append("\n - Caption is empty");
                }
                if (TextUtils.isEmpty(txtCameraMaker.getText().toString())) {
                    strBuilder.append("\n - Camera Marker is empty");
                }
                if (TextUtils.isEmpty(txtCameraModel.getText().toString())) {
                    strBuilder.append("\n - Camera Model is empty");
                }
                if (TextUtils.isEmpty(txtCameraExposure.getText().toString())) {
                    strBuilder.append("\n - Camera Exposure is empty");
                }
                if (TextUtils.isEmpty(txtCameraFstop.getText().toString())) {
                    strBuilder.append("\n - Camera Fstop is empty");
                }
                if (TextUtils.isEmpty(txtCameraIso.getText().toString())) {
                    strBuilder.append("\n - Camera Iso is empty");
                }
                if (TextUtils.isEmpty(tvLocation.getText().toString())) {
                    strBuilder.append("\n - Location is empty");
                }
                if (!TextUtils.isEmpty(strBuilder))
                {
                    strBuilder.insert(0, "There is an empty field");
                }
                strBuilder.append("\n Are you sure to upload?");
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.id_menu_post)
                        .setMessage(strBuilder)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                presenter.addPosting(txtCaption.getText().toString(), lat, lng, tvLocation.getText().toString(), txtCameraMaker.getText().toString(), txtCameraModel.getText().toString(), txtCameraFstop.getText().toString(), txtCameraExposure.getText().toString(), txtCameraIso.getText().toString());
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getContext(), R.string.id_confirm_cancel, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();

                break;
            default:
                break;
        }
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(getContext(), data);
                setLocationText(place.getAddress().toString());
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();

    }
}
