package com.autofocus.www.autofocusnew.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Komentar {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("isi_komentar")
    @Expose
    private String isiKomentar;
    @SerializedName("posting_id")
    @Expose
    private Integer postingId;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("user")
    @Expose
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIsiKomentar() {
        return isiKomentar;
    }

    public void setIsiKomentar(String isiKomentar) {
        this.isiKomentar = isiKomentar;
    }

    public Integer getPostingId() {
        return postingId;
    }

    public void setPostingId(Integer postingId) {
        this.postingId = postingId;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}