package com.autofocus.www.autofocusnew.app.main.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBaseFragment;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.changepassword.ChangePasswordActivity;
import com.autofocus.www.autofocusnew.app.changeprofile.ChangeProfileActivity;
import com.autofocus.www.autofocusnew.app.comment.CommentActivity;
import com.autofocus.www.autofocusnew.app.login.LoginActivity;
import com.autofocus.www.autofocusnew.app.main.MainView;
import com.autofocus.www.autofocusnew.models.Posting;
import com.autofocus.www.autofocusnew.models.UserDataResponse;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.CameraUtil;
import com.autofocus.www.autofocusnew.utils.TokenManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.storage.FirebaseStorage;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import android.support.v7.widget.Toolbar;

import java.io.File;
import java.io.IOException;

public class ProfileFragment extends AppBaseFragment implements ProfileFragmentView {
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.tvPostingCount)
    TextView tvPostingCount;
    @BindView(R.id.tvFollowerCount)
    TextView tvFollowerCount;
    @BindView(R.id.tvFollowingCount)
    TextView tvFollowingCount;
    @BindView(R.id.tvProfileName)
    TextView tvProfileEmail;
    @BindView(R.id.tvProfileEmail)
    TextView tvProfileName;
    @BindView(R.id.btnProfileEdit)
    Button btnProfileEdit;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.profileBio)
    TextView profileBio;
    @BindView(R.id.profileAddress)
    TextView profileAddress;

    @Inject
    public Service service;

    View rootView;
    ProfileFragmentPresenter presenter;
    SharedPreferences preferences;
    private FirebaseStorage firebaseStorage;
    public ProfileFragment() {
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);
        list.setFocusable(false);
        preferences = getContext().getSharedPreferences("prefs", MODE_PRIVATE);
        firebaseStorage = FirebaseStorage.getInstance();
        presenter = new ProfileFragmentPresenter(service, this, preferences, firebaseStorage);
        presenter.getUserProfile();
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getUserProfile();
                swiperefresh.setRefreshing(false);
            }
        });
        setHasOptionsMenu(true);
        int numberOfColumns = 3;
        list.setLayoutManager(new GridLayoutManager(this.getContext(), numberOfColumns));
        return rootView;
    }
//
//    private MainView listener;
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        try {
//            listener = (MainView) context;
//        } catch (ClassCastException castException) {
//            /* The activity does not implement the listener. */
//        }
//    }
//
    @OnClick(R.id.btnProfileEdit)
    void editProfile()
    {
        Intent intent = new Intent(getContext(), ChangeProfileActivity.class);
        startActivity(intent);
    }
    @Override
    public void setProfileImage(RequestOptions requestOptions, String imageurl) {
        Glide.with(getContext())
                .load(imageurl)
                .apply(requestOptions.apply(RequestOptions.circleCropTransform()).diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true))
                .into(imgProfile);
    }

    @Override
    public void setProfileLocal(RequestOptions requestOptions, String path) {
        Glide.with(getContext())
                .load(new File(path))
                .apply(requestOptions.apply(RequestOptions.circleCropTransform()).diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true))
                .into(imgProfile);
    }

    @Override
    public void setPostingCount(String count) {
        tvPostingCount.setText(count);
    }

    @Override
    public void setFollowerCount(String count) {
        tvFollowerCount.setText(count);
    }

    @Override
    public void setFollowingCount(String count) {
        tvFollowingCount.setText(count);
    }

    @Override
    public void setProfileEmail(String email) {
        tvProfileEmail.setText(email);
    }

    @Override
    public void setProfileName(String name) {
        tvProfileName.setText(name);
    }

    @Override
    public void setProfileBio(String bio){
        profileBio.setText(bio);
    }

    @Override
    public void setProfileAdress(String adress) {
        profileAddress.setText(adress);
    }

    @Override
    public void clearImage() {
        CameraUtil.photoClear(getFragmentContext());
    }

    @OnClick(R.id.imgProfile)
    void changeImage()
    {
        dispatchTakePictureIntent();
    }

    File photoFile;
    private void dispatchTakePictureIntent() {
        try {
            photoFile = CameraUtil.createImageFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        startActivityForResult(CameraUtil.photoIntent(getFragmentContext(), photoFile), 5);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if(requestCode == 5) { //TODO uri get path is null
//                Log.i("photo location", photoFile.getAbsolutePath());
                String path = CameraUtil.photoResult(getFragmentContext(), photoFile, data);
                presenter.setProfileImage(path);
            }
        }
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
        getFragmentActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.INVISIBLE);
        getFragmentActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(getContext(), appErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void getPostingList(UserDataResponse response) {
        ProfileFragmentAdapter adapter = new ProfileFragmentAdapter(this.getContext(), response.getData().getPostings(),
                new ProfileFragmentAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(Posting Item) {
//                        Toast.makeText(getContext(), Item.getCaption(),Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getContext(), CommentActivity.class);
                        intent.putExtra("posting_id",Item.getId());
                        startActivity(intent);
                    }
                }
        );
        list.setAdapter(adapter);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_changepassword:
                Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_logout:

                new AlertDialog.Builder(getContext())
                    .setTitle(R.string.id_menu_logout)
                    .setMessage(R.string.id_confirm_logout)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TokenManager.getInstance(preferences).deleteToken();
                            Intent login = new Intent(getContext(), LoginActivity.class);
                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(login);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getContext(), R.string.id_confirm_cancel, Toast.LENGTH_SHORT).show();
                        }
                    })
                    .show();
                break;
            default:
                break;
        }
        return true;
    }
}
