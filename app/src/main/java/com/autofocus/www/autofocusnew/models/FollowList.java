package com.autofocus.www.autofocusnew.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("users_id1")
    @Expose
    private Integer usersId1;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public Integer getUsersId1() {
        return usersId1;
    }

    public void setUsersId1(Integer usersId1) {
        this.usersId1 = usersId1;
    }

}