package com.autofocus.www.autofocusnew.app.main.home;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.models.Posting;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragmentAdapterOld extends RecyclerView.Adapter<HomeFragmentAdapterOld.ViewHolder> {
    private final OnItemClickListener listener;
    private final OnBtnCommentClickListener btnCommentClickListener;
    private List<Posting> data;
    private Context context;

    public HomeFragmentAdapterOld(Context context, List<Posting> data, OnItemClickListener listener, OnBtnCommentClickListener btnCommentClickListener) {
        this.data = data;
        this.listener = listener;
        this.btnCommentClickListener = btnCommentClickListener;
        this.context = context;
    }

    @Override
    public HomeFragmentAdapterOld.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_posting, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        if (getItemCount() <= 0)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_empty, null);
            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT));
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeFragmentAdapterOld.ViewHolder holder, int position) {
        //init event


        holder.click(data.get(position), listener);
        holder.rabStarsclick(data.get(position), holder, listener);
        holder.btnCommentclick(data.get(position), btnCommentClickListener);

        holder.tvPostingCaption.setText(data.get(position).getCaption());
        holder.tvPostingLocation.setText(data.get(position).getLocation());
        holder.tvProfileName.setText(data.get(position).getUser().getName());
        holder.tvTotalRating.setText(String.format(Locale.ENGLISH, "%.1f", data.get(position).getTotalRating()));
        holder.rabStars.setRating(data.get(position).getCurrentUserRating());
        String postingImage = data.get(position).getPhotourl();
        String profileImage = data.get(position).getUser().getPhotourl();
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true);

        Glide.with(context)
                .load(postingImage)
                .apply(requestOptions)
                .into(holder.imgPosting);
        Glide.with(context)
                .load(profileImage)
                .apply(requestOptions.apply(RequestOptions.circleCropTransform()))
                .into(holder.imgProfile);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnItemClickListener {
        void onClick(Posting Item);

        void onStarsChoose(Posting Item, Float rating, ViewHolder holder);
    }

    public interface OnBtnCommentClickListener {
        void onClick(Posting Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.tvPostingCaption)
        TextView tvPostingCaption;
        @Nullable
        @BindView(R.id.tvPostingLocation)
        TextView tvPostingLocation;
        @Nullable
        @BindView(R.id.tvProfileName)
        TextView tvProfileName;
        @Nullable
        @BindView(R.id.tvTotalRating)
        TextView tvTotalRating;
        @Nullable
        @BindView(R.id.imgPosting)
        ImageView imgPosting;
        @Nullable
        @BindView(R.id.imgProfile)
        ImageView imgProfile;
        @Nullable
        @BindView(R.id.rabStars)
        RatingBar rabStars;
        @Nullable
        @BindView(R.id.btnComment)
        Button btnComment;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void btnCommentclick(final Posting data, final OnBtnCommentClickListener listener) {
            btnComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(data);
                }
            });
        }

        public void rabStarsclick(final Posting data,final ViewHolder holder, final OnItemClickListener listener) {
            rabStars.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    listener.onStarsChoose(data, rating,holder);
                }
            });
        }
        public void click(final Posting data, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (rabStars.getVisibility() == View.GONE) {
                        rabStars.setVisibility(View.VISIBLE);
                    } else {
                        rabStars.setVisibility(View.GONE);
                    }
                    listener.onClick(data);
                }
            });
        }

    }

}