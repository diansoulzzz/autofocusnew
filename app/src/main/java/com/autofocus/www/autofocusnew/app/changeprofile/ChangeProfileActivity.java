package com.autofocus.www.autofocusnew.app.changeprofile;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.CameraUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangeProfileActivity extends AppBase implements ChangeProfileView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtBiodata)
    TextView txtBiodata;
    @BindView(R.id.txtAlamat)
    TextView txtAlamat;
    @BindView(R.id.spGender)
    Spinner spGender;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @Inject
    public Service service;
    ChangeProfilePresenter presenter;
    private static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_change_profile);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        presenter = new ChangeProfilePresenter(service, this, preferences, getFirebaseStorage());
        presenter.getUserProfile();
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(getApplicationContext(), appErrorMessage, Toast.LENGTH_LONG).show();
    }

//    @Override
//    public void setProfileImageLocal(Bitmap image) {
//        Glide.with(getApplicationContext()).load(image).apply(requestOptions.apply(RequestOptions.circleCropTransform()).diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true)).into(imgProfile);
//    }

    @Override
    public void setProfileImage(RequestOptions requestOptions, RequestListener listener, String imageurl) {
        Glide.with(getApplicationContext()).load(imageurl).apply(requestOptions.apply(RequestOptions.circleCropTransform()).diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true)).into(imgProfile);
    }

    @Override
    public void setProfileImage(RequestOptions requestOptions, RequestListener listener, byte[] bytes) {
        Glide.with(getApplicationContext()).asBitmap().load(bytes).apply(requestOptions.apply(RequestOptions.circleCropTransform()).diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true)).into(imgProfile);
    }

    @OnClick(R.id.imgProfile)
    void changeImage()
    {
        dispatchTakePictureIntent();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            presenter.setProfileImage(extras);
//        }
        if(resultCode == RESULT_OK) {
            if(requestCode == 5) { //TODO uri get path is null
                Log.i("photo location", photoFile.getAbsolutePath());
                String path = CameraUtil.photoResult(this, photoFile, data);
                //add your own logic for path
                presenter.setProfileImage(path);
            }
        }
    }
//    public static Intent photoIntent(Context context, File photoFile) {
//        List<Intent> cameraIntents = new ArrayList<>();
//        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (photoFile != null)
//            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
//        PackageManager packageManager = context.getPackageManager();
//        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
//        for (ResolveInfo res : listCam) {
//            String packageName = res.activityInfo.packageName;
//            Intent intent = new Intent(captureIntent);
//            intent.setComponent(
//                    new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
//            intent.setPackage(packageName);
//            cameraIntents.add(intent);
//        }
//        Intent galleryIntent = new Intent();
//        galleryIntent.setType("image/*");
//        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
//        Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");
//        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
//                cameraIntents.toArray(new Parcelable[cameraIntents.size()]));
//        return chooserIntent;
//    }

    File photoFile;
    private void dispatchTakePictureIntent() {
        try {
            photoFile = CameraUtil.createImageFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        startActivityForResult(CameraUtil.photoIntent(this, photoFile), 5);
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
//            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//        }
    }

    @Override
    public void setName(String text) {
        txtName.setText(text);
    }

    @Override
    public void setBiodata(String text) {
        txtBiodata.setText(text);
    }

    @Override
    public void setAlamat(String text) {
        txtAlamat.setText(text);
    }

    @Override
    public void setGender(String text) {
        if (text.equalsIgnoreCase("Male")) {
            spGender.setSelection(0);
        } else {
            spGender.setSelection(1);
        }
    }
    @Override
    public void showNameError(CharSequence error) {
        txtName.setError(error);
    }

    @Override
    public void showBiodataError(CharSequence error) {
        txtBiodata.setError(error);
    }

    @Override
    public void showAlamatError(CharSequence error) {
        txtAlamat.setError(error);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.camera_preview, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_next:

                presenter.setProfile(txtName.getText().toString(),spGender.getSelectedItem().toString(),txtBiodata.getText().toString(),txtAlamat.getText().toString());
                break;
            default:
                break;
        }
        return true;
    }
}
