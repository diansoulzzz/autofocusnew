package com.autofocus.www.autofocusnew.app.preview;

public interface PreviewView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void startNextActivity();

    void setPreviewImage(String path);
}
