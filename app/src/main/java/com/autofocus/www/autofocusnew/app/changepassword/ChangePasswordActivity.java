package com.autofocus.www.autofocusnew.app.changepassword;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.AppBase;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.network.Service;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordActivity extends AppBase implements ChangePasswordView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.txtPassword)
    TextView txtPassword;
    @BindView(R.id.txtOldPassword)
    TextView txtOldPassword;
    @BindView(R.id.txtPasswordConfirmation)
    TextView txtPasswordConfirmation;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @Inject
    public Service service;
    ChangePasswordPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_change_password);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        presenter = new ChangePasswordPresenter(service, this, preferences);
//        presenter.getUserProfile();
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(getApplicationContext(), appErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setOldPassword(String text) {
        txtOldPassword.setText(text);
    }

    @Override
    public void setNewPassword(String text) {
        txtPassword.setText(text);
    }

    @Override
    public void setConfirmPassword(String text) {
        txtPasswordConfirmation.setText(text);
    }

    @Override
    public void showOldPasswordError(CharSequence error) {
        txtOldPassword.setError(error);
    }

    @Override
    public void showNewPasswordError(CharSequence error) {
        txtPassword.setError(error);
    }

    @Override
    public void showConfirmPasswordError(CharSequence error) {
        txtPasswordConfirmation.setError(error);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.camera_preview, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_next:
                presenter.changePassword(txtOldPassword.getText().toString(),txtPassword.getText().toString(),txtPasswordConfirmation.getText().toString());
                break;
            default:
                break;
        }
        return true;
    }
}
