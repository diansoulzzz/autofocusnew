package com.autofocus.www.autofocusnew.app.posting.camera;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.autofocus.www.autofocusnew.AppBaseFragment;
import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.app.preview.PreviewActivity;
import com.autofocus.www.autofocusnew.network.Service;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Gesture;
import com.otaliastudios.cameraview.GestureAction;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

public class PostingCameraFragment extends AppBaseFragment implements PostingCameraFragmentView {
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.cameraView)
    CameraView cameraView;
    @BindView(R.id.imgRecord)
    ImageView imgRecord;
    @BindView(R.id.imgSwitch)
    ImageView imgSwitch;
    @Inject
    public Service service;

    View rootView;
    PostingCameraFragmentPresenter presenter;
    SharedPreferences preferences;

    public PostingCameraFragment() {
    }

    public static PostingCameraFragment newInstance() {
        PostingCameraFragment fragment = new PostingCameraFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_posting_camera, container, false);
        ButterKnife.bind(this, rootView);
        preferences = getContext().getSharedPreferences("prefs", MODE_PRIVATE);
        presenter = new PostingCameraFragmentPresenter(service, this, preferences);
        cameraView.mapGesture(Gesture.PINCH, GestureAction.ZOOM); // Pinch to zoom!
        cameraView.mapGesture(Gesture.TAP, GestureAction.FOCUS_WITH_MARKER); // Tap to focus!
        cameraView.mapGesture(Gesture.LONG_TAP, GestureAction.CAPTURE); // Long tap to shoot!
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] picture) {
                CameraUtils.decodeBitmap(picture, new CameraUtils.BitmapCallback() {
                    @Override
                    public void onBitmapReady(Bitmap bitmap) {
                        presenter.saveImage(bitmap);
                    }
                });
            }
        });
        return rootView;
    }

    @Override
    public void startNextActivity(String path) {
        Intent intent = new Intent(rootView.getContext(), PreviewActivity.class);
        intent.putExtra("imagefilepath", path);
        startActivity(intent);
    }

    @OnClick(R.id.imgRecord)
    void onClickRecord() {
//        cameraView.capturePicture();
        cameraView.captureSnapshot();
    }

    @OnClick(R.id.imgSwitch)
    void onClickSwitch() {
        cameraView.toggleFacing();
    }

    @Override
    public void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cameraView.destroy();
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }
}
