package com.autofocus.www.autofocusnew.app.main.search.maps;

import android.content.SharedPreferences;
import android.util.Log;

import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.Posting;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by GBS on 18/01/2018.
 */

public class SearchMapFragmentPresenter {
    private final Service service;
    private final SearchMapFragmentView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "SearchMapPresenter";
    private TokenManager tokenManager;

    public SearchMapFragmentPresenter(Service service, SearchMapFragmentView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }

    public void getPostingList() {
        view.showWait();
        Subscription subscription = service.getPostingList(new Service.GetPostingListCallback() {
            @Override
            public void onSuccess(PostingResponse response) {
                int i = 0;
                RequestOptions requestOptions = new RequestOptions()
                        .centerCrop()
                        .override(200, 200)
                        .placeholder(R.mipmap.ic_launcher_round)
                        .error(R.drawable.ic_search_black_24dp);
                for (Posting posting : response.getData()) {
                    view.addMapMarker(posting,i);
//                    view.downloadAllImage(posting,requestOptions);
                    i++;
                }
                view.getPostingList(response, requestOptions);
                view.removeWait();
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
            }

        }, tokenManager.getToken().getAccessToken());
        subscriptions.add(subscription);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }
}
