package com.autofocus.www.autofocusnew.app.main.profile;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.models.UserDataResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class ProfileFragmentPresenter {
    private final Service service;
    private final ProfileFragmentView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "ProfileFragment";
    private TokenManager tokenManager;
    private FirebaseStorage firebaseStorage;

    public ProfileFragmentPresenter(Service service, ProfileFragmentView view, SharedPreferences preferences, FirebaseStorage firebaseStorage) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
        this.firebaseStorage = firebaseStorage;
    }
    public static String getFileType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public void uploadImage(OnFailureListener failureListener, OnSuccessListener<UploadTask.TaskSnapshot> successListener, String filepath) {
        StorageMetadata metadata = new StorageMetadata.Builder().setContentType(getFileType(filepath)).build();
        String uniqueId = UUID.randomUUID().toString();
        StorageReference imagesRef = firebaseStorage.getReference().child("posting");
        StorageReference fileRef = imagesRef.child(uniqueId);
        BitmapFactory.Options options = new BitmapFactory.Options();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BitmapFactory.decodeFile(filepath, options).compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = fileRef.putBytes(data, metadata);
        uploadTask.addOnFailureListener(failureListener).addOnSuccessListener(successListener);
    }

    public void setImageProfile(final String photourl){
        Subscription subscription = service.setProfileImage(new Service.GetUserCallback() {
            @Override
            public void onSuccess(UserDataResponse response) {
                RequestOptions requestOptions = new RequestOptions();
                view.setProfileImage(requestOptions,response.getData().getPhotourl());
                view.removeWait();
            }

            @Override
            public void onError(NetworkError networkError) {
                view.removeWait();
            }
        }, tokenManager.getToken().getAccessToken(), photourl);
        subscriptions.add(subscription);
        view.clearImage();
    }
    public void setProfileImage(final String filepath) {
        view.showWait();
        uploadImage(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                view.removeWait();
            }
        }, new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                setImageProfile(taskSnapshot.getDownloadUrl().toString());
//                RequestOptions requestOptions = new RequestOptions();
//                view.setProfileLocal(requestOptions,filepath);
            }
        },filepath);
    }

    public void getUserProfile() {
        view.showWait();
        Subscription subscription = service.getUserProfile(new Service.GetUserCallback() {
            @Override
            public void onSuccess(UserDataResponse response) {
                RequestOptions requestOptions = new RequestOptions();
                view.getPostingList(response);
                view.setFollowerCount(response.getData().getFollowerCount().toString());
                view.setFollowingCount(response.getData().getFollowingCount().toString());
                view.setPostingCount(response.getData().getPostingCount().toString());
                if(!TextUtils.isEmpty(response.getData().getPhotourl()))
                {
                    view.setProfileImage(requestOptions, response.getData().getPhotourl());
                }
                view.setProfileEmail(response.getData().getEmail());
                view.setProfileBio(response.getData().getBiodata());
                view.setProfileName(response.getData().getName());
                view.setProfileAdress(response.getData().getAlamat());
                view.removeWait();
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG,errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
            }

        },tokenManager.getToken().getAccessToken());
        subscriptions.add(subscription);
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }
}
