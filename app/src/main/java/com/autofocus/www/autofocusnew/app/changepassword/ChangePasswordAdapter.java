package com.autofocus.www.autofocusnew.app.changepassword;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.autofocus.www.autofocusnew.R;
import com.autofocus.www.autofocusnew.models.Komentar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordAdapter extends RecyclerView.Adapter<ChangePasswordAdapter.ViewHolder> {
    private final OnItemClickListener listener;
    private List<Komentar> data;
    private Context context;

    public ChangePasswordAdapter(Context context, List<Komentar> data, OnItemClickListener listener) {
        this.data = data;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ChangePasswordAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ChangePasswordAdapter.ViewHolder holder, int position) {
        //init event
        holder.click(data.get(position), listener);

        holder.tvProfileName.setText(data.get(position).getUser().getName());
        holder.tvPostingComment.setText(data.get(position).getIsiKomentar());
        String profileImage = data.get(position).getUser().getPhotourl();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        requestOptions.skipMemoryCache(true);
        Glide.with(context)
                .load(profileImage)
                .apply(requestOptions)
                .into(holder.imgProfile);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnItemClickListener {
        void onClick(Komentar Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.tvProfileName)
        TextView tvProfileName;
        @Nullable
        @BindView(R.id.tvPostingComment)
        TextView tvPostingComment;
        @Nullable
        @BindView(R.id.imgProfile)
        ImageView imgProfile;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void click(final Komentar data, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(data);
                }
            });
        }
    }

}