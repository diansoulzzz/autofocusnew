package com.autofocus.www.autofocusnew.app.changeprofile;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.PostingDataResponse;
import com.autofocus.www.autofocusnew.models.UserDataResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class ChangeProfilePresenter {
    private final Service service;
    private final ChangeProfileView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "ChangePasswordPresenter";
    private TokenManager tokenManager;
    private FirebaseStorage firebaseStorage;

    public ChangeProfilePresenter(Service service, ChangeProfileView view, SharedPreferences preferences,FirebaseStorage firebaseStorage) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
        this.firebaseStorage = firebaseStorage;
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }


    public void getUserProfile() {
        view.showWait();
        Subscription subscription = service.getUserProfile(new Service.GetUserCallback() {
            @Override
            public void onSuccess(UserDataResponse response) {
                RequestOptions requestOptions = new RequestOptions();
                view.setName(response.getData().getName());
                view.setAlamat(response.getData().getAlamat());
                view.setBiodata(response.getData().getBiodata());
                view.setGender(response.getData().getGender());
                if (!TextUtils.isEmpty(response.getData().getPhotourl())) {
                    RequestListener listener = new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            view.removeWait();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            view.removeWait();
                            return false;
                        }
                    };
                    view.setProfileImage(requestOptions, listener, response.getData().getPhotourl());
                }

                view.removeWait();
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
            }

        }, tokenManager.getToken().getAccessToken());
        subscriptions.add(subscription);
    }
    public void setProfileImageCameraGallery(String path)
    {
        view.showWait();
    }
    public static String getFileType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }
    public void uploadImage(OnFailureListener failureListener, OnSuccessListener<UploadTask.TaskSnapshot> successListener, String filepath) {
        StorageMetadata metadata = new StorageMetadata.Builder().setContentType(getFileType(filepath)).build();
        String uniqueId = UUID.randomUUID().toString();
        StorageReference imagesRef = firebaseStorage.getReference().child("posting");
        StorageReference fileRef = imagesRef.child(uniqueId);
        BitmapFactory.Options options = new BitmapFactory.Options();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BitmapFactory.decodeFile(filepath, options).compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = fileRef.putBytes(data, metadata);
        uploadTask.addOnFailureListener(failureListener).addOnSuccessListener(successListener);
    }

    public void setProfileImage(String filepath) {
        view.showWait();
        RequestOptions requestOptions = new RequestOptions();
        RequestListener listener = new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                view.removeWait();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                view.removeWait();
                return false;
            }
        };
        view.setProfileImage(requestOptions, listener, filepath);

//        Bitmap imageBitmap = (Bitmap) extras.get("data");
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        RequestOptions requestOptions = new RequestOptions();
//        RequestListener listener = new RequestListener<Drawable>() {
//            @Override
//            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                view.removeWait();
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                view.removeWait();
//                return false;
//            }
//        };
//        view.setProfileImage(requestOptions,listener,stream.toByteArray());
//        view.onFailure("Profile Image was updated");
//        view.showWait();
        this.uploadImage(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                view.removeWait();
            }
        }, new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                uploadImage(taskSnapshot.getDownloadUrl().toString());
                setImageProfile(taskSnapshot.getDownloadUrl().toString());

//                sendPosting(caption,lat,lng,taskSnapshot.getDownloadUrl().toString(),location,camera_maker,camera_model,fstop,exposure_time,isospeed);
            }
        },filepath);
    }
    public void setImageProfile(String photourl){
        Subscription subscription = service.setProfileImage(new Service.GetUserCallback() {
            @Override
            public void onSuccess(UserDataResponse response) {
                view.removeWait();
            }

            @Override
            public void onError(NetworkError networkError) {
                view.removeWait();
            }
        }, tokenManager.getToken().getAccessToken(), photourl);
        subscriptions.add(subscription);
    }
    public void uploadImage(String url)
    {
//        Subscription subscription = service.setUserProfileImage(new Service.GetUserCallback() {
//            @Override
//            public void onSuccess(UserDataResponse response) {
//                Log.d(TAG, "onSuccess");
//                view.setName(response.getData().getName());
//                view.setAlamat(response.getData().getAlamat());
//                view.setBiodata(response.getData().getBiodata());
//                view.setGender(response.getData().getGender());
//                view.removeWait();
//                view.finishActivity();
//            }
//
//            @Override
//            public void onError(NetworkError networkError) {
//                ErrorResponse errorResponse = networkError.getErrorResponse();
//                Log.d(TAG, errorResponse.getMessage());
//                view.removeWait();
//                view.onFailure(errorResponse.getMessage());
////                if (errorResponse.getErrors() != null)
////                {
////                    for (Map.Entry<String, List<String>> error : errorResponse.getErrors().entrySet()) {
////                        if (error.getKey().equals("name")) {
////                            view.showNameError(error.getValue().get(0));
////                        }
////                        if (error.getKey().equals("email")) {
////                            view.showEmailError(error.getValue().get(0));
////                        }
////                        if (error.getKey().equals("password")) {
////                            view.showPasswordError(error.getValue().get(0));
////                        }
////                    }
////                }
//            }
//        }, tokenManager.getToken().getAccessToken(), url);
//        subscriptions.add(subscription);
    }
    public void setProfile(String name, String gender, String biodata, String alamat) {
        view.showWait();
        Subscription subscription = service.setUserProfile(new Service.GetUserCallback() {
            @Override
            public void onSuccess(UserDataResponse response) {
                Log.d(TAG, "onSuccess");
                view.setName(response.getData().getName());
                view.setAlamat(response.getData().getAlamat());
                view.setBiodata(response.getData().getBiodata());
                view.setGender(response.getData().getGender());
                view.onFailure("Profile was updated");
                view.removeWait();
                view.finishActivity();
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
                if (errorResponse.getErrors() != null)
                {
                    for (Map.Entry<String, List<String>> error : errorResponse.getErrors().entrySet()) {
                        if (error.getKey().equals("name")) {
                            view.showNameError(error.getValue().get(0));
                        }
                        if (error.getKey().equals("alamat")) {
                            view.showAlamatError(error.getValue().get(0));
                        }
                        if (error.getKey().equals("biodata")) {
                            view.showBiodataError(error.getValue().get(0));
                        }
                    }
                }
            }
        }, tokenManager.getToken().getAccessToken(), name, gender, biodata, alamat);
        subscriptions.add(subscription);
    }

}
