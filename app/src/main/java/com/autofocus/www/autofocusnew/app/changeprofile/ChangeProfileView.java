package com.autofocus.www.autofocusnew.app.changeprofile;

import android.graphics.Bitmap;

import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;

import java.io.ByteArrayInputStream;

public interface ChangeProfileView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void setProfileImage(RequestOptions requestOptions, RequestListener listener, String imageurl);

    void setProfileImage(RequestOptions requestOptions, RequestListener listener, byte[] bytes);

    void setName(String text);

    void setBiodata(String text);

    void setAlamat(String text);

    void setGender(String text);

    void showNameError(CharSequence error);

    void showBiodataError(CharSequence error);

    void showAlamatError(CharSequence error);

    void finishActivity();
}
