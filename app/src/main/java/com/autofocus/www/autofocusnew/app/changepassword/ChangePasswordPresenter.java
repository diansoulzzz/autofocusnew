package com.autofocus.www.autofocusnew.app.changepassword;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;

import com.autofocus.www.autofocusnew.models.AccessToken;
import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.UserDataResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;
import java.util.Map;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class ChangePasswordPresenter {
    private final Service service;
    private final ChangePasswordView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "ChangePasswordPresenter";
    private TokenManager tokenManager;

    public ChangePasswordPresenter(Service service, ChangePasswordView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }


    public void changePassword(String old_password, String new_password, String confirm_password) {
        view.showWait();
        Subscription subscription = service.changePassword(new Service.GetAccessTokenCallBack() {
            @Override
            public void onSuccess(AccessToken response) {
                tokenManager.deleteToken();
                tokenManager.saveToken(response);
                view.onFailure("Password was updated");
                view.removeWait();
                view.finishActivity();
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());

                if (errorResponse.getErrors() != null)
                {
                    for (Map.Entry<String, List<String>> error : errorResponse.getErrors().entrySet()) {
                        if (error.getKey().equals("old_password")) {
                            view.showOldPasswordError(error.getValue().get(0));
                        }
                        if (error.getKey().equals("password")) {
                            view.showNewPasswordError(error.getValue().get(0));
                        }
                        if (error.getKey().equals("password_confirmation")) {
                            view.showConfirmPasswordError(error.getValue().get(0));
                        }
                    }
                }
            }

        }, tokenManager.getToken().getAccessToken(), old_password, new_password, confirm_password);
        subscriptions.add(subscription);
    }

}
