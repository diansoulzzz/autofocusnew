package com.autofocus.www.autofocusnew.app.posting.camera;

public interface PostingCameraFragmentView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void startNextActivity(String path);
}
