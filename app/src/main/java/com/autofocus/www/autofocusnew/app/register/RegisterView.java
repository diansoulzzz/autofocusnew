package com.autofocus.www.autofocusnew.app.register;

/**
 * Created by GBS on 17/01/2018.
 */

public interface RegisterView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void startNextActivity();

    void showNameError(CharSequence error);

    void showEmailError(CharSequence error);

    void showPasswordError(CharSequence error);

    void showconPasswordEror(CharSequence eror);

}
