package com.autofocus.www.autofocusnew.app.splash;

import rx.subscriptions.CompositeSubscription;

public class SplashPresenter {
    private final SplashView view;
    private CompositeSubscription subscriptions;

    public SplashPresenter(SplashView view) {
        this.view = view;
        this.subscriptions = new CompositeSubscription();
    }
    public void startNextActivity ()
    {
        view.startNextActivity();
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }
}