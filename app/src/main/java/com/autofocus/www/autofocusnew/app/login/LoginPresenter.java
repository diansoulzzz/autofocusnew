package com.autofocus.www.autofocusnew.app.login;

import android.content.SharedPreferences;
import android.util.Log;
import com.autofocus.www.autofocusnew.models.AccessToken;
import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import java.util.List;
import java.util.Map;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public class LoginPresenter {
    private final Service service;
    private final LoginView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "LoginPresenter";
    private TokenManager tokenManager;

    public LoginPresenter(Service service, LoginView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
        if (this.tokenManager.getToken().getAccessToken() != null) {
            view.startNextActivity();
//            this.tokenManager.deleteToken();
        }
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

    public void attemptLogin(String username,String password)
    {
        view.showWait();
        Subscription subscription = service.getTokenLogin(new Service.GetAccessTokenCallBack() {
            @Override
            public void onSuccess(AccessToken response) {
                Log.d(TAG,"onSuccess");
                tokenManager.saveToken(response);
                view.removeWait();
                view.startNextActivity();
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG,errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
                if (errorResponse.getErrors() != null)
                {
                    for (Map.Entry<String, List<String>> error : errorResponse.getErrors().entrySet()) {
                        if (error.getKey().equals("email")) {
                            view.showEmailError(error.getValue().get(0));
                        }
                        if (error.getKey().equals("password")) {
                            view.showPasswordError(error.getValue().get(0));
                        }
                    }
                }
            }
        },username,password);
        subscriptions.add(subscription);
    }
}
