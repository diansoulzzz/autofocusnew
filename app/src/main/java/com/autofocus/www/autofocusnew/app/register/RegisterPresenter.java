package com.autofocus.www.autofocusnew.app.register;

import android.content.SharedPreferences;
import android.util.Log;

import com.autofocus.www.autofocusnew.app.login.LoginView;
import com.autofocus.www.autofocusnew.models.AccessToken;
import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import java.util.List;
import java.util.Map;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class RegisterPresenter {
    private final Service service;
    private final RegisterView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "LoginPresenter";
    private TokenManager tokenManager;

    public RegisterPresenter(Service service, RegisterView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
        if (this.tokenManager.getToken().getAccessToken() != null) {
            view.startNextActivity();
        }
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

    public void attemptRegister(String name, String username,String password,String conPassword)
    {
        view.showWait();
        Subscription subscription = service.getTokenRegister(new Service.GetAccessTokenCallBack() {
            @Override
            public void onSuccess(AccessToken response) {
                Log.d(TAG,"onSuccess");
                tokenManager.saveToken(response);
                view.removeWait();
                view.startNextActivity();
            }
            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG,errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
                if (errorResponse.getErrors() != null)
                {
                    for (Map.Entry<String, List<String>> error : errorResponse.getErrors().entrySet()) {
                        if (error.getKey().equals("name")) {
                            view.showNameError(error.getValue().get(0));
                        }
                        if (error.getKey().equals("email")) {
                            view.showEmailError(error.getValue().get(0));
                        }
                        if (error.getKey().equals("password")) {
                            view.showPasswordError(error.getValue().get(0));
                        }
                        if (error.getKey().equals("password_confirmation")) {
                            view.showconPasswordEror(error.getValue().get(0));
                        }
                    }
                }
            }
        },name,username,password,conPassword);
        subscriptions.add(subscription);
    }
}
