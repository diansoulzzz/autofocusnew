package com.autofocus.www.autofocusnew.app.posting.gallery;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.autofocus.www.autofocusnew.R;
import com.bumptech.glide.Glide;
import java.io.File;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PostingGalleryFragmentAdapter extends RecyclerView.Adapter<PostingGalleryFragmentAdapter.ViewHolder> {
    private final OnItemClickListener listener;
    private List<String> data;
    private Context context;

    public PostingGalleryFragmentAdapter(Context context, List<String> data, OnItemClickListener listener) {
        this.data = data;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public PostingGalleryFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_posting_gallery, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(PostingGalleryFragmentAdapter.ViewHolder holder, int position) {
        //init event
        holder.OnItemClick(data.get(position), listener);
        Glide.with(context).load(new File(data.get(position))).into(holder.imgGallery);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public interface OnItemClickListener {
        void onClick(String Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.imgGallery)
        ImageView imgGallery;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void OnItemClick(final String data, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(data);
                }
            });
        }
    }


}