package com.autofocus.www.autofocusnew.app.posting.gallery;


public interface PostingGalleryFragmentView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void startNextActivity(String path);
}
