package com.autofocus.www.autofocusnew;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.autofocus.www.autofocusnew.di.components.AppComponent;
import com.autofocus.www.autofocusnew.di.components.DaggerAppComponent;
import com.autofocus.www.autofocusnew.network.NetworkModule;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;

import java.io.File;

public class AppBase extends AppCompatActivity implements AppBaseFragment.Callback {

    AppComponent daggerInterface;
    FirebaseStorage fStorage;
    FirebaseAuth fAuth;
    private static Context context = null;
    private final String TAG = "AppBase";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        File cacheFile = new File(getCacheDir(), "responses");
        daggerInterface = DaggerAppComponent.builder().networkModule(new NetworkModule(cacheFile)).build();
        fAuth = FirebaseAuth.getInstance();
        signInAnonymously();
        fStorage = FirebaseStorage.getInstance();
    }

    public AppComponent getAppComponent() {
        return daggerInterface;
    }

    public FirebaseStorage getFirebaseStorage() {
        return fStorage;
    }

    public static Context getContext() {
        return context;
    }

    @Override
    public void onFragmentAttached() {

    }

    private void signInAnonymously() {
        fAuth.signInAnonymously().addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {

            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.e(TAG, "signInAnonymously:FAILURE", exception);
            }
        });
    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {android.Manifest.permission.READ_CONTACTS, android.Manifest.permission.WRITE_CONTACTS, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS, android.Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION};
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }
}