package com.autofocus.www.autofocusnew.app.main;

/**
 * Created by GBS on 18/01/2018.
 */

public interface MainView {

    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void setBottomMenuIndex(Integer index);


}
