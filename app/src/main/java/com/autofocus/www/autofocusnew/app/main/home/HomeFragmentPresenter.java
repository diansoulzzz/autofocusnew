package com.autofocus.www.autofocusnew.app.main.home;

import android.content.SharedPreferences;
import android.util.Log;

import com.autofocus.www.autofocusnew.models.ErrorResponse;
import com.autofocus.www.autofocusnew.models.PostingDataResponse;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.network.NetworkError;
import com.autofocus.www.autofocusnew.network.Service;
import com.autofocus.www.autofocusnew.utils.TokenManager;

import java.util.Locale;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by GBS on 18/01/2018.
 */

public class HomeFragmentPresenter {
    private final Service service;
    private final HomeFragmentView view;
    private CompositeSubscription subscriptions;
    private final String TAG = "SearchMapFragment";
    private TokenManager tokenManager;

    public HomeFragmentPresenter(Service service, HomeFragmentView view, SharedPreferences preferences) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.tokenManager = TokenManager.getInstance(preferences);
    }

    public void getPostingList() {
        view.showWait();
        Subscription subscription = service.getPostingList(new Service.GetPostingListCallback() {
            @Override
            public void onSuccess(PostingResponse cityListResponse) {
                view.removeWait();
                view.getPostingList(cityListResponse);
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
            }
        }, tokenManager.getToken().getAccessToken());
        subscriptions.add(subscription);
    }

    public void setRating(Integer posting_id, Float stars, final HomeFragmentAdapter.ViewHolderPosting holder) {
        view.showWait();
        Subscription subscription = service.setPostingRating(new Service.GetPostingCallback() {
            @Override
            public void onSuccess(PostingDataResponse response) {
                holder.tvTotalRating.setText(String.format(Locale.ENGLISH, "%.1f", response.getData().getTotalRating()));
                view.removeWait();
            }

            @Override
            public void onError(NetworkError networkError) {
                ErrorResponse errorResponse = networkError.getErrorResponse();
                Log.d(TAG, errorResponse.getMessage());
                view.removeWait();
                view.onFailure(errorResponse.getMessage());
            }

        }, tokenManager.getToken().getAccessToken(), posting_id, stars);
        subscriptions.add(subscription);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }
}
