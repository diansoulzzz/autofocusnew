package com.autofocus.www.autofocusnew.app.main.search.users;

import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.models.UserResponse;

/**
 * Created by GBS on 18/01/2018.
 */

public interface SearchUserFragmentView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

//    void setBtnFollowState(String string,Integer backgroundcolor, Integer textcolor);

    void getUserList(UserResponse response);
}
