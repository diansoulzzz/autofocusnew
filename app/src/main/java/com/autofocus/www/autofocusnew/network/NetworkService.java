package com.autofocus.www.autofocusnew.network;

import com.autofocus.www.autofocusnew.models.AccessToken;
import com.autofocus.www.autofocusnew.models.CityListResponse;
import com.autofocus.www.autofocusnew.models.PostingDataResponse;
import com.autofocus.www.autofocusnew.models.PostingResponse;
import com.autofocus.www.autofocusnew.models.UserDataResponse;
import com.autofocus.www.autofocusnew.models.UserResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface NetworkService {
    @GET("v1/city")
    Observable<CityListResponse> getCityList();

    @POST("api/register")
    @FormUrlEncoded
    Observable<AccessToken> register(@Field("name") String name, @Field("email") String email, @Field("password") String password, @Field("password_confirmation") String conPassword);

    @POST("api/login")
    @FormUrlEncoded
    Observable<AccessToken> login(@Field("email") String username, @Field("password") String password);

    @POST("api/refresh")
    @FormUrlEncoded
    Observable<AccessToken> refresh(@Field("refresh_token") String refreshToken);

    @GET("api/posts")
    Observable<PostingResponse> getPostingList(@Header("Authorization") String authorization);

    @GET("api/usersfollow")
    Observable<UserResponse> getUserList(@Header("Authorization") String authorization);

    @GET("api/postsId")
    Observable<PostingDataResponse> getPostingWhereId(@Header("Authorization") String authorization, @Query("posting_id") Integer posting_id);

    @GET("api/userprofile")
    Observable<UserDataResponse> getUserProfile(@Header("Authorization") String authorization);

    @POST("api/changepassword")
    @FormUrlEncoded
    Observable<AccessToken> changePassword(@Header("Authorization") String authorization, @Field("old_password") String old_password, @Field("password") String password, @Field("password_confirmation") String confirm_password);

    @POST("api/addcomment")
    @FormUrlEncoded
    Observable<PostingDataResponse> addCommentGetPosting(@Header("Authorization") String authorization, @Field("posting_id") Integer posting_id, @Field("comment") String comment);

    @POST("api/tooglefollow")
    @FormUrlEncoded
    Observable<UserResponse> toogleFollowGetUserList(@Header("Authorization") String authorization, @Field("users_id") Integer users_id);

    @POST("api/addposting")
    @FormUrlEncoded
    Observable<PostingResponse> addPosting(@Header("Authorization") String authorization, @Field("caption") String caption, @Field("lat") Double lat, @Field("lng") Double lng, @Field("photourl") String photourl, @Field("location") String location, @Field("camera_maker") String camera_maker, @Field("camera_model") String camera_model, @Field("fstop") String fstop, @Field("exposure_time") String exposure_time, @Field("isospeed") String isospeed);

    @POST("api/updateuserprofile")
    @FormUrlEncoded
    Observable<UserDataResponse> setUserProfile(@Header("Authorization") String authorization, @Field("name") String name, @Field("gender") String gender, @Field("biodata") String biodata, @Field("alamat") String alamat);

    @POST("api/setpostingrating")
    @FormUrlEncoded
    Observable<PostingDataResponse> setPostingRating(@Header("Authorization") String authorization, @Field("posting_id") Integer posting_id, @Field("stars") Float stars);

    @POST("api/setimageprofile")
    @FormUrlEncoded
    Observable<UserDataResponse> setImageProfile(@Header("Authorization") String authorization, @Field("photourl") String photourl);

}